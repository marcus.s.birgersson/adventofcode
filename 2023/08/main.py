import math
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def parse_data(lines):
    data = {}
    for line in lines:
        line = line.split(" = ")
        node = line[0]
        next_nodes = line[1][1:-1].split(", ")
        data[node]=next_nodes
    return data

def solve_day1(fname):
    data = read_lines(fname)
    directions = data[0]
    data = data[2:]
    data = parse_data(data)
    L=len(directions)
    step = 0
    n = 'AAA'
    t=0
    while n!='ZZZ':
        if step>=L:
            step = 0
        next_dir = directions[step]
        if next_dir=='L':
            n = data[n][0]
        else:
            n = data[n][1]

        step+=1
        t+=1
        
    return t
    
def get_start_nodes(data):
    nodes = []
    for n in data:
        if n.endswith('A'):
            nodes.append(n)
    return nodes

def get_end_nodes(data):
    nodes = []
    for n in data:
        if n.endswith('Z'):
            nodes.append(n)
    return nodes

def is_finish(nodes):
    for n in nodes:
        if not n.endswith('Z'):
            return False
    return True


def find_path(data, directions, start):
    visited = {}
    path = {}
    t = 0
    step = 0
    node = start
    while True:
        if step>=len(directions):
            step = 0
        next_dir = directions[step]

        if (node,step) in visited:
            break
        else:
            visited[(node,step)]=True
        if not node in path:
            path[node]=[]
        path[node].append(t)

        if next_dir=='L':
            node = data[node][0]
        else:
            node = data[node][1]
        step+=1
        t+=1
   
    # print(path)
    return path

            
def solve_day2(fname):
    data = read_lines(fname)
    directions = data[0]

    data = data[2:]
    data = parse_data(data)
    nodes = get_start_nodes(data)
    end_nodes = get_end_nodes(data)

    all_paths = {}
    for start in nodes:
        path = find_path(data, directions, start)
        all_paths[start] = path

    filtered_paths = {}
    for start in nodes:
        filtered = {}
        path = all_paths[start]
        # print(path)
        for p in path:
            if p.endswith('Z'):
                filtered[p]=path[p]
        filtered_paths[start]=filtered

    steps = []
    for n in nodes:
        for e in filtered_paths[n]:
            steps.append(filtered_paths[n][e][0])
    
    prod = 1
    for s in steps:
        prod*=s
    factors = []
    i=3
    while i<prod:
        if prod%i==0:
            factors.append(i)
            prod//=i
        elif prod%2==0:
            factors.append(2)
            prod//=2
        else:
            i+=2
    factors = list(set(factors))
    prod = 1
    for f in factors:
        prod*=f
    return prod

if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    res2 = solve_day2('input')
    print("Result part2:", res2)
    assert res1==17287
    assert res2==18625484023687
