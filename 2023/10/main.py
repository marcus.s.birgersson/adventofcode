import math
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines


def find_char(lines, char):
    for y in range(len(lines)):
        for x in range(len(lines[0])):
            if lines[y][x]==char:
                return (x,y)
    
def _find_loop(lines, x0,y0,x1,y1,d):
    path = [(x0,y0)]
    while x0!=x1 or y0!=y1:
        path.append((x1,y1))
        c = lines[y1][x1] 
        if c=='|':
            if d=='up':
                y1-=1
            elif d=='down':
                y1+=1
            else:
                return None
        elif c=='-':
            if d=='left':
                x1-=1
            elif d=='right':
                x1+=1
            else:
                print("ERROR")
        elif c=='L':
            if d=='down':
                d='right'
                x1+=1
            elif d=='left':
                d='up'
                y1-=1
            else:
                print('error')
            
        elif c=='J':
            if d=='right':
                d='up'
                y1-=1
            elif d=='down':
                d='left'
                x1-=1
            else:
                return None
        elif c=='7':
            if d=='right':
                d='down'
                y1+=1
            elif d=='up':
                d='left'
                x1-=1
            else:
                return None
        elif c=='F':
            if d=='left':
                d='down'
                y1+=1
            elif d=='up':
                d='right'
                x1+=1
            else:
                return None
        elif c=='.':
            return None
        else:
            return None

    path.append((x1,y1))
    return path
    
from colorama import Fore
def print_map(lines, loop=None):
    for y in range(len(lines)):
        for x in range(len(lines[y])):
            c = lines[y][x]
            if c=='S':
                print(Fore.RED + c, end='')
            elif loop and (x,y) in loop:
                print(Fore.GREEN + c, end='')
            else:
                print(Fore.WHITE + c, end='')
        print()

def find_loop(lines):
    x0,y0 = find_char(lines, 'S')
    next_start = [(x0+1,y0), (x0-1, y0), (x0, y0+1), (x0,y0-1)]
    next_dir = ['right', 'left', 'down', 'up']
    for i in range(len(next_start)):
        x1 = next_start[i][0]
        y1 = next_start[i][1]
        ndir = next_dir[i]
        path = _find_loop(lines,x0,y0,x1,y1,ndir)
        if path:
            return path

def solve_day1(fname):
    lines = read_lines(fname)
    loop = find_loop(lines)
    res = (len(loop)-1)//2
    return res
        
def solve_day2(fname):
    lines = read_lines(fname)
    loop = find_loop(lines)

    # Shoelace formula for area of polygon
    # https://en.wikipedia.org/wiki/Shoelace_formula
    area = 0
    for i in range(len(loop)-1):
        x0 = loop[i][0]
        x1 = loop[i+1][0]
        y0 = loop[i][1]
        y1 = loop[i+1][1]
        area+=((y0+y1)*(x0-x1))
    area/=2
    # Pick's theorem for inner poins of polygon
    # https://en.wikipedia.org/wiki/Pick's_theorem
    b = len(loop)-1
    i = area+1-b/2
    return round(i)

if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    res2 = solve_day2('input')
    print("Result part2:", res2)
    assert res1==7093
    assert res2==407
