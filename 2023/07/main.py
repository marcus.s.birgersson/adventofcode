import math

# Python program for implementation of MergeSort

# Merges two subarrays of arr[].
# First subarray is arr[l..m]
# Second subarray is arr[m+1..r]

def merge(arr, l, m, r, joker=False):
    n1 = m - l + 1
    n2 = r - m
 
    # create temp arrays
    L = [0] * (n1)
    R = [0] * (n2)
 
    # Copy data to temp arrays L[] and R[]
    for i in range(0, n1):
        L[i] = arr[l + i]
 
    for j in range(0, n2):
        R[j] = arr[m + 1 + j]
 
    # Merge the temp arrays back into arr[l..r]
    i = 0     # Initial index of first subarray
    j = 0     # Initial index of second subarray
    k = l     # Initial index of merged subarray
 
    while i < n1 and j < n2:
        # if L[i] <= R[j]:
        if comp(L[i],R[j], joker=joker)<0:
            arr[k] = L[i]
            i += 1
        else:
            arr[k] = R[j]
            j += 1
        k += 1
 
    # Copy the remaining elements of L[], if there
    # are any
    while i < n1:
        arr[k] = L[i]
        i += 1
        k += 1
 
    # Copy the remaining elements of R[], if there
    # are any
    while j < n2:
        arr[k] = R[j]
        j += 1
        k += 1

# l is for left index and r is right index of the
# sub-array of arr to be sorted
 
 
def mergeSort(arr, l, r, joker=False):
    if l < r:
 
        # Same as (l+r)//2, but avoids overflow for
        # large l and h
        m = l+(r-l)//2
 
        # Sort first and second halves
        mergeSort(arr, l, m, joker=joker)
        mergeSort(arr, m+1, r, joker=joker)
        merge(arr, l, m, r, joker=joker)

def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def transpose(lines):
    trans = list(map(list, zip(*lines)))
    lines = []
    for line in trans:
        lines.append(''.join(line))
    return lines

def parse_data(lines, joker=False):
    data = [] 
    for line in lines:
        line = line.split(" ")
        line[1] = int(line[1])
        t = get_type(line[0], joker=joker)
        line.append(t)
        data.append(line)
    return data

def is_equal(a1, a2):
    if len(a1)!=len(a2):
        return False
    for i in range(len(a1)):
        if a1[i]!=a2[i]:
            return False
    return True

def comp(c1, c2, joker=False):
    ranks = {'five':6, 'four':5, 'fullhouse':4, 'three':3, 'twopair':2, 'pair':1, 'highcard':0}
    t1=c1[2]
    t2=c2[2]
    if not ranks[t1]==ranks[t2]:
        if ranks[t1]>ranks[t2]:
            return 1
        else:
            return -1
    card_values = {'A':14, 'K':13, 'Q':12, 'J':11, 'T':10, '9':9, '8':8, '7':7, '6':6, '5':5, '4':4, '3':3, '2':2}
    if joker:
        card_values = {'A':14, 'K':13, 'Q':12, 'T':10, '9':9, '8':8, '7':7, '6':6, '5':5, '4':4, '3':3, '2':2, 'J':1}

    for i in range(len(c1[0])):
        if c1[0][i]==c2[0][i]:
            continue
        v1 = card_values[c1[0][i]]
        v2 = card_values[c2[0][i]]

        if v1>v2:
            return 1
        else:
            return -1
    return 0
    
def get_type(card, joker=False):
    card = sorted(card)
    counts = {}
    for v in card:
        if v in counts:
            counts[v]=counts[v]+1
        else:
            counts[v]=1

    
    nJokers = 0
    if joker:
        if 'J' in counts:
            nJokers = counts['J']
            if nJokers==5:
                return 'five'
            del counts['J']

    values = list(counts.values())
    values.sort(reverse=True)

    while nJokers>0:
        for i in range(len(values)):
            if values[i]<5:
                values[i]+=1
                nJokers-=1
                break
            elif i>=len(values)-1:
                print("Error!!!")
                exit(0)
                nJokers=0
    if values[0]==5:
        return 'five'
    elif values[0]==4:
        return 'four'
    elif values[0]==3:
        if values[1]==2:
            return 'fullhouse'
        return 'three'
    elif values[0]==2:
        if values[1]==2:
            return 'twopair'
        return 'pair'
    return 'highcard'

    
def solve_day1(fname):
    lines = read_lines(fname)
    cards = parse_data(lines)
    mergeSort(cards, 0, len(cards)-1)
    score = 0
    for i in range(len(cards)):
        rank = i+1
        bid = cards[i][1]
        score+=rank*bid
    return score

def solve_day2(fname):
    lines = read_lines(fname)
    joker = True
    cards = parse_data(lines, joker=joker)
    mergeSort(cards, 0, len(cards)-1, joker=joker)

    tmp_cards = parse_data(lines)
    prev_card_map = {}
    for c in tmp_cards:
        prev_card_map[c[0]] = c[2]

    score = 0
    for i in range(len(cards)):
        rank = i+1
        bid = cards[i][1]
        score+=rank*bid

    return score

if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    res2 = solve_day2('input')
    print("Result part2:", res2)
    assert res1 == 248396258
    assert res2== 246436046

    # Wrong: 246567881
    
