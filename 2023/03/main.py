
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def get_num_str(string, start):
    while start-1>=0 and string[start-1].isnumeric():
        start-=1
    num_str = string[start]
    index = start+1
    for index in range(start+1, len(string)):
        if string[index].isnumeric():
            num_str+=string[index]
        else:
            break
        # index+=1
    return num_str
        
def has_adj_sym(x_start, x_end, y_start, y_end, lines):
    if x_start<0:
        x_start=0
    if y_start<0:
        y_start=0
    if x_end>len(lines[0])-1:
        x_end=len(lines[0])-1
    if y_end>len(lines)-1:
        y_end=len(lines)-1

    for y in range(y_start, y_end+1):
        for x in range(x_start, x_end+1):
            if lines[y][x].isnumeric() or lines[y][x]=='.':
                continue
            else:
                return True
    return False
            

def solve_day1(fname):
    lines = read_lines(fname)
    inc_numbers = []
    for y in range(len(lines)):
        x=0
        while x < len(lines[y]):
            c=lines[y][x]
            if c=='.':
                x+=1
                continue
            elif c.isnumeric():
                num_str = get_num_str(lines[y], x)
                is_adj = has_adj_sym(x-1, x+len(num_str), y-1, y+1, lines)
                if is_adj:
                    inc_numbers.append(int(num_str))
                x+=len(num_str)
            else:
                x+=1
    return sum(inc_numbers)

def get_adj_num(x,y, lines):
    numbers = []
    coor= [(x-1, y-1), (x,y-1), (x+1, y-1), (x-1,y), (x+1,y), (x-1, y+1), (x,y+1, x+1, y+1)]

    # Check vertical first
    if lines[y][x-1].isnumeric():
        numbers.append(get_num_str(lines[y], x-1))
    if lines[y][x+1].isnumeric():
        numbers.append(get_num_str(lines[y], x+1))

    # Check above
    if y-1>=0:
        if lines[y-1][x].isnumeric():
            numbers.append(get_num_str(lines[y-1], x))
        else:
            # check upper corners
            if lines[y-1][x-1].isnumeric():
                numbers.append(get_num_str(lines[y-1], x-1))
            if lines[y-1][x+1].isnumeric():
                numbers.append(get_num_str(lines[y-1], x+1))
            
    # Check below
    if y+1<len(lines):
        if lines[y+1][x].isnumeric():
            numbers.append(get_num_str(lines[y+1], x))
        else:
            # check lower corners
            if lines[y+1][x-1].isnumeric():
                numbers.append(get_num_str(lines[y+1], x-1))
            if lines[y+1][x+1].isnumeric():
                numbers.append(get_num_str(lines[y+1], x+1))
    return numbers

def solve_day2(fname):
    lines = read_lines(fname)
    gear_ratios = []
    for y in range(len(lines)):
        x=0
        for x in range(len(lines[y])):
            c=lines[y][x]
            if c=='*':
                numbers = get_adj_num(x,y, lines)
                if len(numbers)==2:
                    gear_ratios.append(int(numbers[0])*int(numbers[1]))

    return sum(gear_ratios)

if __name__=='__main__':
    res1 = solve_day1('input')
    res2 = solve_day2('input')
    print("Result part1:", res1)
    print("Result part2:", res2)
    assert res1==537732
    assert res2==84883664
