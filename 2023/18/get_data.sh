#!/bin/bash

day=${PWD##*/}
source ../.env
# SESSION="53616c7465645f5f36fe55b38acc6c08a1994fb11cecd3c963c4f8e9ab7d3aaabbfef86cdbe8c0a429b6f8525055006e9162a20be4f6e98ecad9844bbb4e28ab"

printf $day
https://adventofcode.com/
# wget --load-cookies cookies.txt https://adventofcode.com/2023/day/17/input
wget --no-cookies https://adventofcode.com/2023/day/$day/input --header "Cookie: session=$SESSION"
# https://adventofcode.com/2023/day/17/input
