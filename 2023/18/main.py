import math
import numpy as np
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def parse_data(lines):
    data = [] 
    for line in lines:
        line = line.split(" ")
        d = line[0]
        n = int(line[1])
        c = line[2][1:-1]
        data.append((d,n,c))
    return data

def polygon_area(array):
    # Shoelace formula for area of polygon
    # https://en.wikipedia.org/wiki/Shoelace_formula
    area = 0
    for i in range(len(array)-1):
        x0,y0 = array[i]
        x1,y1 = array[i+1]
        area+=((y0+y1)*(x0-x1))
    return abs(area)/2

def poly_len(array):
    length = 0
    for i in range(len(array)-1):
        length+=abs(array[i][0]-array[i+1][0])+abs(array[i][1]-array[i+1][1])
    return length
        
def compute_area(coord):
    area = polygon_area(coord)
    b = poly_len(coord)
    i = area+1-b/2
    return i+b

def solve_day1(fname):
    lines = read_lines(fname)
    data = parse_data(lines)

    coord = create_coordinates(data)
    return round(compute_area(coord))

    # x,y=(0,0)
    # coord = []
    # coord.append((x,y))
    # dir = {'D':(0,1), 'U':(0,-1), 'L':(-1,0), 'R':(1,0)}
    # x_vec = [0]
    # y_vec = [0]
    # for d,n,_ in data:
    #     dx,dy = dir[d]
    #     x = x+dx*n
    #     y = y+dy*n
    #     coord.append((x,y))
    # return round(compute_area(coord))

def create_coordinates(data):
    x,y=(0,0)
    coord = []
    coord.append((x,y))
    dir = {'D':(0,1), 'U':(0,-1), 'L':(-1,0), 'R':(1,0)}
    for ins in data:
        d = ins[0]
        n = ins[1]
        dx,dy = dir[d]
        x = x+dx*n
        y = y+dy*n
        coord.append((x,y))
    return coord
 
def extract_instructions(data):
    ins = []
    directions = ['R', 'D', 'L', 'U']
    for _,_,c in data:
        c = c[1:]
        d = directions[int(c[-1])]
        n = int(c[:-1], 16)
        ins.append((d,n))

    return ins
def solve_day2(fname):
    lines = read_lines(fname)
    data = parse_data(lines)
    ins = extract_instructions(data)
    coord = create_coordinates(ins)
    return round(compute_area(coord))

if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    res2 = solve_day2('input')
    print("Result part2:", res2)
    assert res1 == 36807
    assert res2==48797603984357
