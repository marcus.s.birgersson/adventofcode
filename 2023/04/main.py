import math
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def solve_day1(fname):
    lines = read_lines(fname)
    score = []
    for line in lines:
        line = line.split(": ")
        card_nr = int(line[0].split(" ")[-1])
        line = line[1].split(" | ")
        correct_num = list(filter(lambda x: x.isnumeric(), line[0].split(" ")))
        correct_num = [int(x.strip()) for x in correct_num]
        my_num = list(filter(lambda x: x.isnumeric(), line[1].split(" ")))
        my_num = [int(x) for x in my_num]
        n_matches = 0
        for n in my_num:
            if n in correct_num:
                n_matches+=1
        if n_matches>0:
            score.append(math.pow(2,n_matches-1))
        else:
            score.append(0)
    return int(sum(score))

class Card:
    def __init__(self, nr, n_matches, n_instances):
        self.nr = nr
        self.matches = n_matches
        self.inst = n_instances

    def inc(self,n):
        self.inst+=n

    def get_matches(self):
        return self.matches

    def get_inst(self):
        return self.inst
        
def parse_data(lines):
    cards = {}
    for line in lines:
        line = line.split(": ")
        card_nr = int(line[0].split(" ")[-1])
        line = line[1].split(" | ")
        correct_num = list(filter(lambda x: x.isnumeric(), line[0].split(" ")))
        correct_num = [int(x.strip()) for x in correct_num]
        my_num = list(filter(lambda x: x.isnumeric(), line[1].split(" ")))
        my_num = [int(x) for x in my_num]
        n_matches = 0
        for n in my_num:
            if n in correct_num:
                n_matches+=1
        cards[card_nr] = Card(card_nr, n_matches, 1)

    return cards

def solve_day2(fname):
    lines = read_lines(fname)
    score = []
    cards = parse_data(lines)
    for card_nr in cards.keys():
        card = cards[card_nr]
        inc = card.get_inst()
        for i in range(1, card.matches+1):
            nr = card_nr+i
            cards[nr].inc(inc)

    n_cards = 0        
    for card_nr in cards:
        card = cards[card_nr]
        
        n_cards+=card.get_inst()

    return n_cards

if __name__=='__main__':
    res1 = solve_day1('input')
    res2 = solve_day2('input')
    print("Result part1:", res1)
    print("Result part2:", res2)
    assert res1==33950
    assert res2==14814534
