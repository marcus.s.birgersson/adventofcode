
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def solve_day1(fname):
    data = read_lines(fname)
    sum = 0
    for line in data:
        line = list(filter(lambda x: x.isnumeric(), line))
        num = int(''.join([line[0], line[-1]]))

        sum+=num
    return sum

def substitute_num_string(string):
    str_num_map = {'one':'1', 'two':'2', 'three':'3', 'four':'4', 'five':'5', 'six':'6', 'seven':'7', 'eight':'8', 'nine':'9'}
    all_indexes = []
    for s in str_num_map.keys():
        index = string.find(s)
        indexes = [i for i in range(len(string)) if string.startswith(s, i)]
        if len(indexes)>0:
            for i in indexes:
                all_indexes.append((i,str_num_map[s]))

    string = list(string)
    if len(all_indexes)>0:
        for i,s in all_indexes:
            string[i]=s
        
    return string

def solve_day2(fname):
    data = read_lines(fname)
    sum = 0
    for line in data:
        line = substitute_num_string(line)
        line = list(filter(lambda x: x.isnumeric(), line))
        num = int(''.join([line[0], line[-1]]))

        sum+=num
    return sum

if __name__=='__main__':
    res1 = solve_day1('input')
    res2 = solve_day2('input')
    print("Result part1:", res1)
    print("Result part2:", res2)
    assert res1==54239
    assert res2==55343
