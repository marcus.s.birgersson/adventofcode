import math
from queue import PriorityQueue

def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def transpose(lines):
    trans = list(map(list, zip(*lines)))
    lines = []
    for line in trans:
        lines.append(''.join(line))
    return lines

def parse_data(lines):
    data = [] 
    return data

def is_equal(a1, a2):
    if len(a1)!=len(a2):
        return False
    for i in range(len(a1)):
        if a1[i]!=a2[i]:
            return False
    return True

def find_char(lines,char):
    for y in range(len(lines)):
        for x in range(len(lines[0])):
            if lines[y][x]==char:
                return (x,y)

def compute_distances(lines):
    (x0,y0) = find_char(lines,'S')
    to_visit = PriorityQueue()
    visited = set()

    to_visit.put((0,x0,y0))
    visited.add((x0,y0))

    max_x = len(lines[0])
    max_y = len(lines)
    distance = {}
    while to_visit.qsize()>0:
        d,x,y = to_visit.get()
        distance[(x,y)]=d

        # print("Current distance:", d, 'x=',x, 'y=',y)
        # print("x_max:", max_x, "y_max:", max_y)
        # print("to_visit:", to_visit.qsize())
        # if to_visit.qsize()>18000:
            # exit(0)
        delta = [(0,1), (0,-1), (1,0), (-1,0)]
        for dx,dy in delta:
            x1=x+dx
            y1=y+dy
            # print("New x,y:", x1,y1)
            if x1<0 or x1>=max_x:
                continue
            if y1<0 or y1>=max_y:
                continue
            if lines[y1][x1]=='#':
                continue
            if (x1,y1) in visited:
                # print("Already visited")
                continue
            # print("Adding points", d+1, x1,y1)
            to_visit.put((d+1, x1,y1))
            visited.add((x1,y1))

                
    return distance
           

def compute_posible_tiles(dist, nsteps, rad=0):
    filtered = {}
    is_even = (nsteps%2==0)
    tiles = 0
    for k,v in dist.items():
        if v<=nsteps:
            if (is_even and v%2==0) or (not is_even and v%2!=0):
                # n=1
                # print()
                # print("Include dist=", v, 'with max steps=', nsteps)
                tiles+=1
                # if rad:
                    # print("Could be repeated, rad=", rad, 'max steps:',nsteps)
                    # print("nsteps/rep:", nsteps/rad)
                        # exit(0)
                        # pass
                        
                    # if v==0:
                        # v+=rep
                    # fac = rep//v
                    # print("Add with repeating factor:", fac)
                    
                filtered[k]=v
                # print("Adding n=",n, "Total tiles:", tiles)
            if rad>0 and ((is_even and (v+rad)%2==0) or (not is_even and (v+rad)%2!=0)):
                if v+rad<nsteps:
                    n=(nsteps//(v+rad))
                    # n*=2
                    print(k,v)
                    print("Is repeated", (nsteps//(v+rad)),'times')
                    print("New n:", n)

                    tiles+=n

    return filtered
    return tiles
    return len(filtered)
                

def solve_day1(fname):
    lines = read_lines(fname)
    # print(lines)
    dist = compute_distances(lines)
    n = compute_posible_tiles(dist, 64)
    # print(n)
    return n
    # print(dist)
    # data = parse_data(lines)
        
def print_map(lines):
    for l in lines:
        print(l)

def extend_map(lines, full=False):
    vert_extended = lines+lines+lines
    extended = []
    nS = 0
    for l in vert_extended:
        extended.append(l+l+l)

    skip = (len(lines)-1)//2
    if full:
        skip=0

    clean = []
    for y in range(len(extended)-skip):
        # print(y)
        if y<skip:
            continue
        if skip>0:
            row = list(extended[y])[skip:-skip]
        else:
            row = list(extended[y])
        # print("row:", row)
        for x in range(len(row)):
            # print(row[x])
            if row[x]=='S':
                # print("Found S")
                if nS!=4:
                    # pass
                    row[x]='.'
                nS+=1
        clean.append(''.join(row))

    # print_map(clean)
    # exit(0)
    return clean
    # return extended
from collections import defaultdict

def solve_day2(fname):
    init_lines = read_lines(fname)

    # print_map(lines)
    r = (len(init_lines)-1)//2
    lines = extend_map(init_lines)
    dist = compute_distances(lines)
    n_steps = 50
    n1 = compute_posible_tiles(dist, n_steps)
    print(len(n1))
    # print_map(lines)
    full = extend_map(init_lines, full=True)
    full = extend_map(full, full=True)
    full_dist = compute_distances(full)
    # print_map(full)
    n2 = compute_posible_tiles(full_dist, n_steps)
    print(len(n2))
    print()
    # print_map(full)
    print("Rad:", r)
    n1_dist = defaultdict(int)
    n2_dist = defaultdict(int)
    for k,d in n1.items():
        n1_dist[d]+=1
    for k,d in n2.items():
        n2_dist[d]+=1

    # for k,v in n1.items():
        # print('n1 data:', k,v)
        # break
    # print()
    # Add repetitions:
    n1_new = n1_dist.copy()
    for k,d in n1.items():
        init_d = d
        # print("n1 data:", k,d)
        # exit(0)
        if d<r:
            continue
        while d+r<=n_steps:
            if (d+r)%2==0:
                print("Updating values for:",k, 'init d=', init_d)
                print("Add value for d=",d+r)
                n1_new[d+r]+=1
            d+=r
        exit(0)
        # n1_dist[d]+=1

    print(n1_dist)
    print(n2_dist)
    print("New n1:")
    print(n1_new)
    exit(0)
    exit(0)
    rad = (len(lines)-1)/2
    lines = extend_map(lines)
    print_map(lines)
    # exit(0)
    dist = compute_distances(lines)
    # print(dist[(32,32)])
    # exit()
    # n = compute_posible_tiles(dist, 13101)
    print("Size: (X,Y):", len(lines[0]), len(lines))
    # max_steps = len(lines)//2
    # if max_steps%2==0:
    #     max_steps+=1
    # print("Max steps:", max_steps)
    # n = compute_posible_tiles(dist, max_steps)

    steps = 100
    print("Steps:", steps)
    n = compute_posible_tiles(dist, steps, rad=rad)
    print("Tiles:", n)
    steps = 26501365
    # print(steps/65)

if __name__=='__main__':
    # res1 = solve_day1('input')
    # print("Result part1:", res1)
    res2 = solve_day2('test')
    # print("Result part2:", res2)
    # assert res1 == 3532
    # assert res2== 
