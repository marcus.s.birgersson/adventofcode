import math
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines


def print_map(lines):
    for line in lines:
        for c in line:
            print(c, end='')
        print()

def parse_data(lines):
    data = []
    for line in lines:
        d = line.split(" ")
        springs = list(d[0])
        groups = [int(x) for x in d[1].split(',')]
        data.append((springs,groups))
        
    return data

def create_comb(springs):
    stack = []
    comb = []
    stack.append(springs)
    # print("Original:", springs)
    while len(stack)>0:
        s = stack.pop()
        for i in range(len(s)):
            if s[i]=='?':
                s2 = s.copy()
                s2[i]='.'
                stack.append(s2)
                s[i]='#'
        # print("Done with combination:")
        # print(s)
        comb.append(s)
    # print("Done computing combinations:")
    return comb

def is_invalid(semi_springs, groups):
    group_index = 0
    group_size = 0
    for s in semi_springs:
        if s=='#':
            group_size+=1
        elif s=='.':
            if group_size>0:
                if group_index>=len(groups):
                    # print("Too many groups?:", 'group_index=', group_index, 'len groups:',len(groups), 'Acctual:', group_size)
                    # print(semi_springs)
                    # print(groups)
                    return True
                    exit(0)
                elif groups[group_index]!=group_size:
                    # print("Found invalid:", 'group_index=', group_index, 'group_size:',groups[group_index], 'Acctual:', group_size)
                    # print(semi_springs)
                    # print(groups)
                    return True
                else:
                    group_size=0
                    group_index+=1
        elif s=='?':
           return False
    return False
def create_valid_comb(springs, groups):
    stack = []
    comb = []
    ncomb=0
    stack.append(springs)
    # print("Original:", springs)
    while len(stack)>0:
        s = stack.pop()
        # print(len(stack))
        # print("Pop s:", s)
        group_size = 0
        group_index = 0

        for i in range(len(s)):
            # print("i=", i)
            if s[i]=='#':
                # print("Extend group size")
                group_size+=1
            elif s[i]=='.':
                if group_size>0:
                    if groups[group_index]!=group_size:
                        break
                    group_size=0
                    group_index+=1
                    
            elif s[i]=='?':
                # print("Found ?")
                if group_size>0:
                    if group_index>=len(groups):
                        break
                    if group_size==groups[group_index]:
                        # print("Group is large enough, end group with .")
                        s[i]='.'
                        group_size=0
                        group_index+=1
                    elif group_size<groups[group_index]:
                        # print(s)
                        # print('Group not large enough, extend group with #')
                        s[i]='#'
                        # print("s=",s)
                        group_size+=1
                    else:
                        # print("Error, group too large")
                        # print("i=", i)
                        # print("s=",s)
                        # print('Groups:', groups)
                        # print("Group index:", group_index, 'groups[group_index]=', groups[group_index])
                        # print("Current group size:", group_size)
                        break
                        exit(0)
                else: 
                    # print("Create copy with .")
                    s2 = s.copy()
                    s2[i]='.'
                    
                    invalid = is_invalid(s2, groups)
                    if not invalid:
                        stack.append(s2)
                        
                    s[i]='#'
                    group_size+=1
                    # print("Start new group with #")
                    # print("Group size=", group_size)
                    if is_invalid(s, groups):
                        break
        # print("Done with combination:")
        # print(s)
        # print(groups)

        # print("Check valid:", s)
        # correct = ['#','.','#','.','#','#','#','.']*5
        # correct = correct[:-1]
        # print("Correct:", correct)
        # if is_equal(s, correct):
            # print("FOund it")
            # print("Is valid:", is_valid(s, groups))
            # exit(0)
        if is_valid(s, groups):
            # print("Found valid")
            comb.append(s)
            ncomb+=1
    # print("valiiDone computing combinations:")
    return comb

def is_equal(a1, a2):
    if len(a1)!=len(a2):
        return False
    for i in range(len(a1)):
        if a1[i]!=a2[i]:
            return False
    return True

def is_valid(springs, groups):
    ngroups = 0
    group_size = 0
    group_index = 0
    group_data = []
    for s in springs:
        # print(s)
        if s=='#':
            group_size+=1
        elif s=='.':
            if group_size>0:
               group_data.append(group_size) 
               group_size=0
    if group_size>0:
        group_data.append(group_size) 
        group_size=0

    # print("Is valid:", springs, groups)
    # if len(groups)==len(group_data):
        # print("Groups:", groups)
        # print("Group data:", group_data)
        # print()
    return is_equal(group_data, groups)
          
def get_valid(comb, groups):
    valid = []
    for c in comb:
        if is_valid(c, groups):
            valid.append(c)
    return valid
def solve_day1(fname):
    lines = read_lines(fname)
    data = parse_data(lines)
    res = []
    comb = []
    for i in range(len(data)):
        mem={}
        n = find_number_of_combinations(data[i][0], data[i][1])
        comb.append(n)
    return sum(comb)
        
def unfold(data):
    unfolded = []
    for d in data:
        springs = d[0]
        springs.append('?')
        springs = d[0]*5
        springs = springs[:-1]
        groups = d[1]*5
        unfolded.append((springs,groups))

    return unfolded

mem = {}
def find_number_of_combinations(springs, groups):
    global mem
    group_size = 0
    group_index = 0
    s = springs
    if (str(s),str(groups)) in mem:
        return mem[(str(s),str(groups))]
    for i in range(len(s)):
        if s[i]=='#':
            group_size+=1
        elif s[i]=='.':
            if group_size>0:
                if groups == []:
                    return 0
                if groups[group_index]!=group_size: # Is invalid
                    return 0
                nsprings = springs[i+1:]
                ngroups = groups[group_index+1:]
                n = find_number_of_combinations(nsprings, ngroups)
                mem[(str(s),str(groups))] = n
                return n
                    
        elif s[i]=='?':
            s1 = s.copy()
            s2 = s.copy()
            s1[i]='.'
            s2[i]='#'
            ns1 = find_number_of_combinations(s1,groups)
            mem[(str(ns1),str(groups))] = ns1
            ns2 = find_number_of_combinations(s2,groups)
            mem[(str(ns2),str(groups))] = ns2
            return ns1+ns2
    if is_valid(s, groups):
        return 1
    else:
        return 0

    
def solve_day2(fname):
    global mem
    lines = read_lines(fname)
    data = parse_data(lines)
    data = unfold(data)
    
    total_comb = 0
    comb = []
    # for d in data:
    i = 5
    for i in range(len(data)):
        mem={}
        n = find_number_of_combinations(data[i][0], data[i][1])
        print("Found combinations:", n, "(",i,'/',len(data),')')
        comb.append(n)
    print()
    for i in range(len(data)):
        print(data[i])
        print("combinations for row",i, 'is', comb[i])
        print()
    print("Total combinations:", sum(comb))
    return sum(comb)


    # res = []
    # total_comb = 0
    # for d in data:
    #     # d = data[3]
    #     # print("Original:", d)
    #     comb = create_valid_comb(d[0], d[1])
    #     # print("Valid combinations:")
    #     # print(comb)
    #     # print(d[1])
    #     # break
    #     # valid = get_valid(comb, d[1])
    #     print("Valid combinations:", comb)
    #     # print()
    #     # exit(0)
    #     total_comb+=comb
    #     # res.append(len(comb))
    #     # print(len(valid))
    #     # break
    #     # break
        
    # # print("Sum:", sum(res))
    # print(total_comb)
    # return total_comb
    # return sum(res)

if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    # res2 = solve_day2('input')
    # print("Result part2:", res2)
    assert res1 == 7204
    # assert res2==1672318386674
