
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def parse_data(lines):
    data = []
    for line in lines:
        line = line.split(": ")
        game_id = int(line[0].split(" ")[-1])

        line = line[1]
        rounds = line.split("; ")
        parsed_rounds = []
        for round in rounds:
            parsed_round = {}
            round = round.split(', ')
            for entry in round:
                entry = entry.split(" ")
                n = int(entry[0])
                c = entry[1]
                parsed_round[c]=n
            parsed_rounds.append(parsed_round)
        data.append((game_id, parsed_rounds))

    return data


def check_valid(conf, rounds):
    for round in rounds:
        for key in round.keys():
            if round[key]>conf[key]:
                return False

    return True

def solve_day1(fname):
    lines = read_lines(fname)
    data = parse_data(lines)
    conf = {'red':12, 'green':13, 'blue':14}
    valid_games = []
    for d in data:
        if check_valid(conf, d[1]):
           valid_games.append(d[0])
    return sum(valid_games)

def find_min(game):
    min_c = game[0]
    game=game[1:]
    for g in game:
        for c in g.keys():
            if c not in min_c:
                min_c[c] = g[c]
            elif min_c[c]<g[c]:
                min_c[c]=g[c]
    return min_c

def compute_power(conf):
    p = 1
    for k in conf.keys():
        p*=conf[k]
    return p

def solve_day2(fname):
    lines = read_lines(fname)
    data = parse_data(lines)
    min_conf = []
    for d in data:
        m = find_min(d[1])
        min_conf.append(m)

    power = []
    for m in min_conf:
        power.append(compute_power(m))

    return sum(power)

if __name__=='__main__':
    res1 = solve_day1('input')
    res2 = solve_day2('input')
    print("Result part1:", res1)
    print("Result part2:", res2)
    assert res1==2156
    assert res2==66909
