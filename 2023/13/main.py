import math
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def transpose(lines):
    trans = list(map(list, zip(*lines)))
    lines = []
    for line in trans:
        lines.append(''.join(line))
    return lines

def parse_data(lines):
    data = [] 
    tmp = []
    for l in lines:
        if l=='':
            data.append(tmp)
            tmp=[]
        else:
            tmp.append(l)
    data.append(tmp)
    return data

def is_equal(a1, a2):
    if len(a1)!=len(a2):
        return False
    for i in range(len(a1)):
        if a1[i]!=a2[i]:
            return False
    return True

def find_vert_mirror(data):
    found = False
    row = 0
    for x in range(1,len(data[row])):
        for y in range(len(data)):
            left = x-1
            right = x
            is_mirror=True
            while left>=0 and right<len(data[y]):
                if data[y][left]!=data[y][right]:
                    is_mirror=False
                    break
                left-=1
                right+=1

            if not is_mirror:
                break
        if is_mirror:
            return x
            break
    return None
                
def find_hor_mirror(data):
    data = transpose(data)
    return find_vert_mirror(data)

def find_vert_mirror_smudge(data):
    row = 0
    for x in range(1,len(data[row])):
        smudge_fix = False
        for y in range(len(data)):
            left = x-1
            right = x
            is_mirror=True
            while left>=0 and right<len(data[y]):
                if data[y][left]!=data[y][right]:
                    if not smudge_fix:
                        # Try fixing the smudge and continue
                        smudge_fix = True
                    else:
                        is_mirror=False
                        break
                left-=1
                right+=1

            if not is_mirror:
                break
        if is_mirror and smudge_fix:
            return x
            break
    return None

def find_hor_mirror_smudge(data):
    data = transpose(data)
    return find_vert_mirror_smudge(data)

def solve_day1(fname):
    lines = read_lines(fname)
    data = parse_data(lines)
    score = 0
    for i in range(len(data)):
        d = data[i]
        mirror = find_vert_mirror(d)
        if not mirror:
            mirror = find_hor_mirror(d)
            score = score + mirror*100
        else:
            score+=mirror
    return score
        
def solve_day2(fname):
    lines = read_lines(fname)
    data = parse_data(lines)
    score = 0
    for i in range(len(data)):
        d = data[i]
        mirror = find_vert_mirror_smudge(d)
        if not mirror:
            mirror = find_hor_mirror_smudge(d)
            score = score + mirror*100
        else:
            score+=mirror
    return score

if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    res2 = solve_day2('input')
    print("Result part2:", res2)
    assert res1 == 28651
    assert res2== 25450 
