import math
from queue import PriorityQueue

def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def transpose(lines):
    trans = list(map(list, zip(*lines)))
    lines = []
    for line in trans:
        lines.append(''.join(line))
    return lines

def parse_data(lines):
    data = [] 
    for l in lines:
        data.append([int(x) for x in l])
    return data

def is_equal(a1, a2):
    if len(a1)!=len(a2):
        return False
    for i in range(len(a1)):
        if a1[i]!=a2[i]:
            return False
    return True

def part2_skip_cond(d, new_dir, nd, new_nd):
    return ((nd<4 and new_dir!=d or new_nd>10) and d>-1)

def part1_skip_cond(d, new_dir, nd, new_nd):
    return new_nd>3

def find_min_path(lines, x0, y0, skip_cond):
    mem = {}
    xf=len(lines[0])-1
    yf=len(lines)-1

    q = PriorityQueue()
    s = lines[y0][x0]
    q.put((0,0,0,-1,-1))

    while not q.empty():
        s,x,y,d,nd = q.get()
        if (x,y,d,nd) in mem:
            continue
        mem[(x,y,d,nd)]=s

        if x==xf and y==yf:
            return s

        for new_dir,(dx,dy) in enumerate([(-1,0), (0,1), (1,0), (0,-1)]):

            if (new_dir+2)%4==d:
                continue #reverse direction
            new_nd = (1 if new_dir!=d else nd+1)            

            if skip_cond(d, new_dir, nd, new_nd):
                continue

            x1 = x+dx
            y1 = y+dy
            if (0<=x1<len(lines[0]) and 0<=y1<len(lines)):
                new_s = s+lines[y1][x1]
                q.put((new_s,x1,y1,new_dir, new_nd))

    
def solve_day1(fname):
    lines = read_lines(fname)
    lines = parse_data(lines)
    p = find_min_path(lines, 0, 0, part1_skip_cond)
    return p

        
def solve_day2(fname):
    lines = read_lines(fname)
    lines = parse_data(lines)
    p = find_min_path(lines, 0,0, part2_skip_cond)
    return p

if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    res2 = solve_day2('input')
    print("Result part2:", res2)
    assert res1 == 1044
    assert res2== 1227
