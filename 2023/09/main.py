import math
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
        # data = file.read()
    # print("File data:", data)
    return lines
    # return lines

def is_zeros(array):
    for a in array:
        if a!=0:
            return False
    return True

def predict_next(serie):
    diff = []
    # prev_diff=[]
    # print(serie)
    for i in range(len(serie)-1):
        diff.append(serie[i+1]-serie[i])
    if is_zeros(diff):
        return serie[-1] 
    else:
        return serie[-1]+predict_next(diff)

def predict_prev(serie):
    diff = []
    for i in range(len(serie)-1):
        diff.append(serie[i+1]-serie[i])
    if is_zeros(diff):
        return serie[0] 
    else:
        return serie[0]-predict_prev(diff)

def format_data(lines):
    data = []
    for line in lines:
        data.append([int(x) for x in line.split(" ")])

    return data

def solve_day1(fname):
    lines = read_lines(fname)
    data = format_data(lines)
    predictions = []
    for d in data:
        prediction = predict_next(d)
        predictions.append(prediction)

    return sum(predictions)
        
def solve_day2(fname):
    lines = read_lines(fname)
    data = format_data(lines)
    predictions = []
    for d in data:
        prediction = predict_prev(d)
        predictions.append(prediction)

    return sum(predictions)

if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    res2 = solve_day2('input')
    print("Result part2:", res2)
    assert res1==1974232246
    assert res2==928
# 1974232257 too high?
