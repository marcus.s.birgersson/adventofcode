import math
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def transpose(lines):
    trans = list(map(list, zip(*lines)))
    lines = []
    for line in trans:
        lines.append(''.join(line))
    return lines

def parse_data(lines):
    data = {}
    for line in lines:
        line = line.split(" -> ")
        name = line[0]
        if name=='broadcaster':
            t='broadcaster'
        else:
            t=name[0]
            name = name[1:]

        targets = [t.strip() for t in line[1].split(',')]
        d = {'targets':targets, 'type':t}

        if t=='&':
            d['state']={}
        elif t=='%':
            d['state']='off'


        # print(name,":", d)
        data[name] = d
        # print(line)
        # print(name, 'targets:', targets, 'type:', t)
        # data[name] = {'targets':targets, 'type':t}
    for k,v in data.items():
        targets = v['targets']
        # print("Node:", k)
        # print(targets)
        for t in targets:
            if t in data and data[t]['type']=='&':
                # print("Is conjunction:", t)
                # print("Add input", k, "to", t)
                # print("Init:", data[t])
                data[t]['state'][k]='low'
            
    return data

def is_equal(a1, a2):
    if len(a1)!=len(a2):
        return False
    for i in range(len(a1)):
        if a1[i]!=a2[i]:
            return False
    return True

def press_button(data, record_node=None):
    total_pulses = ['low']
    targets = data['broadcaster']['targets']
    pulses = ['low']*len(targets)
    sources = ['broadcaster']*len(targets)
    recorded = {}
    # print("Init targets:", targets)
    # print("Init pulses:", pulses)
    while len(targets)>0:
        total_pulses+=pulses
        new_targets = []
        new_sources = []
        # print("Current pulses:", total_pulses)
        # Update targets state:
        # print(pulses)
        for name,pulse,source in zip(targets,pulses,sources):
            # print(source, "send pulse",pulse,"to", name)
            # if name=='output':
            if name not in data:
                # print("Send to output, ignore")
                # total_pulses.append(pulse)
                # print("Total pulses:", total_pulses)
                continue
            t = data[name]
            # print("Init target:", t)
            # print(name, pulse)
            if t['type']=='%': # Is flipp-flopp
                # print("Is flipp-flopp")
                if pulse=='low':
                    # print("Toggle state")
                    t['state'] = 'on' if t['state']=='off' else 'off'
                    new_targets.append(t)
                    new_sources.append(name)

            elif t['type']=='&':
                # print("To be implemented")
                # print("Init t:", t)
                # print("Source:", source)
                # print("Input name:", name)

                # t['state'].append(pulse)
                t['state'][source] = pulse
                new_targets.append(t)
                new_sources.append(name)
                # print("New t:", t)
                # exit(0)
                # exit(0)
            # print(t)

        # Check specific targets
        # print(data['rs'])
        if record_node:
            record_states = data[record_node]['state']
            for k,v in record_states.items():
                if k not in recorded:
                    recorded[k]='low'
                if v=='high':
                    recorded[k]='high'
            
        targets = []
        pulses = []
        sources = []
        for t,s in zip(new_targets,new_sources):

            if t['type']=='%':
                next_targets = t['targets']
                if t['state']=='on':
                    pulse = 'high'
                else:
                    pulse = 'low'
                new_pulses = [pulse]*len(next_targets)
                targets+=next_targets
                pulses+=new_pulses
                sources+=[s]*len(next_targets)
                # print("Pulses:", pulses, 'to', next_targets)
            elif t['type']=='&':
                # print("Not implemented")
                # print("Send pulse from", t)
                in_pulses = list(set(t['state'].values()))
                # print("In pulses:", in_pulses)
                if len(in_pulses)==1 and in_pulses[0]=='high':
                    pulse='low'
                else:
                    pulse='high'
                # t['state']=[]
                next_targets = t['targets']
                new_pulses = [pulse]*len(next_targets)
                # print("Next targets:", next_targets)
                # print("Next pulses:", new_pulses)
                targets+=next_targets
                pulses+=new_pulses
                sources+=[s]*len(next_targets)
                # print("sources:", sources)
                # print("")
   
                # exit(0)

        # print("New targets:", targets)
        # print("New pulses:", pulses)

        # break
    # print("Final states")
    # for k,v in data.items():
        # print(k,v)
    # print("Total final pulses:", total_pulses)
    return data, total_pulses, recorded

def solve_day1(fname):
    lines = read_lines(fname)
    data = parse_data(lines)
    # for d in data:
    #     print(d, data[d])
    # exit(0)
    total_pulses = []
    for i in range(1000):
        data, pulses,_ = press_button(data)
        total_pulses+=pulses
    # print("Final states:")
    # for k,v in data.items():
        # print(k,v)
    

    nhigh=0
    nlow=0
    for p in total_pulses:
        if p=='high':
            nhigh+=1
        elif p=='low':
            nlow+=1
        else:
            print("Error")
            exit(0)
        # print(p)
    # print("high:",nhigh, 'Low:', nlow)
    # print("Answear:", nhigh*nlow)
    return nhigh*nlow

        

        
# def solve_day2(fname):
#     lines = read_lines(fname)
#     data = parse_data(lines)
def solve_day2(fname):
    lines = read_lines(fname)
    data = parse_data(lines)
    # init_len = len(data)
    # data = filter_data(data,'rx')
    # print(init_len, len(data))
    nodes = []
    for k,v in data.items():
        if 'rx' in v['targets']:
            nodes = list(v['state'])
            break
            # exit(0)

    # print(nodes)

    # bt_init = data['bt']['state'].copy()
    init_states = {}
    has_changed = {}
    for n in nodes:
        # print(data[n])
        init_states[n]=data[n]['state']
        has_changed[n]=False
        
    # print(init_states)
    # print(has_changed)
    # exit(0)
    # bt_changed = False

    # print(bt_init)
    npresses = 0
    cycles = {}
    for n in nodes:
        cycles[n]=0
    # exit(0)
    # while npresses<10000:
    for npresses in range(1,10000):
        # print(npresses)
        data, pulses,recorded = press_button(data, 'rs')
        # if npresses==3738:
            # print('Recorded:', recorded)
            # exit(0)
        for n,v in recorded.items():
            if v=='high':
                cycles[n]=npresses
                # print("Found new:", cycles)

        found_all = 0 not in list(set(cycles.values()))
        # print(found_all)
        if found_all:
            break


    fac = 1
    for n,v in cycles.items():
        fac*=v
    factors = []
    d=3
    while fac>1:
        if fac%2==0:
            factors.append(2)
            fac//=2
        elif fac%d==0:
            factors.append(d)
            fac//=d
        d+=2
    factors = list(set(factors))
    answear = 1
    for f in factors:
        answear*=f
    return answear
            




if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    res2 = solve_day2('input')
    print("Result part2:", res2)
    assert res1 == 834323022
    assert res2== 225386464601017

