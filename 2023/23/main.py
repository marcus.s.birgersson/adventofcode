import math

def print(*args, **kwargs):
    debug=True
    # debug=False
    if debug:
        # __builtins__.print('My overridden print() function!')
        return __builtins__.print(*args, **kwargs)
    
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def transpose(lines):
    trans = list(map(list, zip(*lines)))
    lines = []
    for line in trans:
        lines.append(''.join(line))
    return lines

def parse_data(lines):
    data = [] 
    return data

def is_equal(a1, a2):
    if len(a1)!=len(a2):
        return False
    for i in range(len(a1)):
        if a1[i]!=a2[i]:
            return False
    return True


from colorama import Fore
def print_path(p,m, full=False):
    # xv = [path[0] for path in p]
    yv = [path[1] for path in p]
    # min_x = max(min(xv)-10), 0)
    if not full:
        min_y = max(p[-1][1]-10, 0)
        max_y = min(p[-1][1]+10, len(m))
    else:
        min_y = 0
        max_y = len(m)
    print(min_y, max_y)
    
                
    for y in range(min_y,max_y):
        for x in range(len(m[0])):
            if (x,y)==p[-1]:
                col = Fore.RED
                ch="X"
            elif (x,y) in p:
                col = Fore.GREEN
                ch = "o"
            else:
                col = Fore.WHITE
                ch = m[y][x]
            print(col + ch, end='')
        print()
from queue import PriorityQueue
def find_hikes(x0,y0, m, slopes=True):
    print("Start at", x0,y0)
    hikes = []
    deadend = []
    visited = {}
    q = [[(x0,y0)]]
    print(q)

    while len(q)>0:
        p = q.pop()

        x,y = p[-1]

        print("Len q:", len(q), "Position:", x,y)

        if len(p)>1:
            xp,yp = p[-2]
            if (xp,yp,x,y) in deadend:
                print("Found deadend, skip")
                # print_path(p,m)
                # input('wait')
                continue
            if (xp,yp,x,y) in visited and visited[(xp,yp,x,y)]==len(p):
                print("Found visited")
                # print_path(p,m)
                # input('wait')
                # print("Aready been here, skip")
                # exit(0)
                continue
            visited[(xp,yp,x,y)]=len(p)

        c = m[y][x]

        if c in ['<', '>', 'v', '^'] and slopes:
            dp = {'<':[(-1,0)], '>':[(1,0)], 'v':[(0,1)], '^':[(0,-1)]}[c]
        elif c=='.' or slopes:
            dp = [(-1,0),(1,0),(0,1),(0,-1)]

        # print("Current position:", x,y)
        # print("Current tile:", c)
        # print("Len q:", len(q))
        # input("Press to continue...")
        # print("dp:", dp)
        nxt = []
        for dx,dy in dp:
            # print(dx, dy)
            x1 = x+dx
            y1 = y+dy
            if (x,y,x1,y1) in deadend:
                print("Found start of deadend, skip")
                # print_path(p,m)
                # input('wait')
                # print(x1,y1)
                continue
                # exit(0)

            if m[y1][x1]!='#' and (x1,y1) not in p:
                if 0<=x1<len(m[0]) and 0<=y1<len(m):
                    # print("Appending:",x,y,m[y1][x1])
                    nxt.append((x1,y1))

        # print("Next positions:", nxt)
        # 17 5, 19 29, 11 59 13 81, 11 113, 43 133, 29, 101, 31 77, 41 63, 41 33, 33 13, 67 5, 59 35, 55 63, 55 87, 63 101, 
        # 63 125, 87 131, 87 103, 81 81, 81 65, 87 39, 81 9, 111 19, 109 33, 99 53, 101 79, 101 111, 103 129, 127 129, 131 111,
        # 135 83, 137 61, 137 29, 131 111
        if len(nxt)>1:
            print("Found multiple next points", len(nxt))
            print(x,y)
            # print_path(p,m)
            # input('wait')
            # input("waiting")
        if len(nxt)==0:
            print("Found dead end at", x,y, m[y][x])

            # Backtrack to nearest crossing
            dp = [(-1,0),(1,0),(0,1),(0,-1)]
            ending = ''
            for dx,dy in dp:
                xt = x+dx
                yt = y+dy
                print(yt,xt,m[yt][xt])
                ending+=m[yt][xt]
            print("Ending:", ending)
            is_deadend = ending.count('#')==3
            print("Is deadend:",is_deadend) 
            # exit(0)


            if True or is_deadend:
                # input("wait")
                print("Backtrack from",x,y)
                found_deadend = False
                for i in range(len(p)):
                    if found_deadend:
                        break
                    x0,y0 = p[-i]
                    print("Check", x0,y0)
                    for dx,dy in dp:
                        xt = x0+dx
                        yt = y0+dy
                        if (xt,yt) in p:
                            continue
                        elif m[yt][xt]=='.':
                            print("Found crossing at", xt,yt)
                            print("Points to add:")
                            xp,yp = p[-i+1]
                            print(x0,y0,xp,yp)
                            deadend.append((x0,y0,xp,yp))
                            print(deadend)
                            # exit(0)
                            
                            
                            # for j in range(len(p[-i:])):
                            #     xd,yd=p[-i+j]
                            #     print("Add", xd,yd)
                            #     deadend.append((xd,yd))
                            found_deadend = True
                            # exit(0)
                            # break

                            # exit(0)

            # exit()
        
        for x1,y1 in nxt:
            pp = p.copy()
            pp.append((x1,y1))
            if y1==len(m)-1:
                print("Found goal!", x1,y1)
                # print_path(p,m)
                # input('wait')
                hikes.append(pp)
            else:
                # print("To append:", pp)
                if pp in q:
                    print("Found extra q")
                    exit()
                q.append(pp)
                    
            
        # print("Next:", nxt)
    # for h in hikes:
        # print(len(h))
    hikes_len = [len(h)-1 for h in hikes]
    # print(hikes_len)
    hikes_len = sorted(hikes_len, reverse=True)
    # print(hikes_len)
    return hikes_len[0]


            
        
        # Find possible next paths
    
def find_intersections(lines):
    intersections = []
    dp = [(-1,0),(1,0),(0,1),(0,-1)]
    for y in range(len(lines)):
        for x in range(len(lines[0])):
            if lines[y][x]=='.':
                tiles = ''
                for dx,dy in dp:
                    xp = x+dx
                    yp = y+dy
                    if 0<=xp<len(lines[0]) and 0<=yp<len(lines):
                        tiles+=lines[yp][xp]
                if tiles.count('.')>2:
                    # print("intersection at:", x,y)
                    # print(tiles)
                    intersections.append((x,y))
                    # return intersections
                
    return intersections
            

def solve_day1(fname):
    lines = read_lines(fname)
    # data = parse_data(lines)
    # for l in lines:
        # print(l)
    return find_hikes(1,0, lines)
        
def remove_slopes(lines):
    # for l in lines:
    #     print(l)
    # print()
    new_lines = []
    for y in range(len(lines)):
        row = list(lines[y])
        for x in range(len(row)):
            if row[x] in ['>','<','v','^']:
                row[x]='.'
        new_lines.append(''.join(row))

    # for l in new_lines:
    #     print(l)
                
    # exit(0)
    return new_lines

def find_path(start, intersections, lines):
    # print("Init páth:", start)
    path = start
    dp = [(-1,0),(1,0),(0,1),(0,-1)]
    while True:
        x,y = path[-1]
        # print("Prev:", x,y)
        for dx,dy in dp:
            xp,yp = x+dx, y+dy
            if 0<=xp<len(lines[0]) and 0<=yp<len(lines) and lines[yp][xp]=='.':
                if (xp,yp) in path:
                    # print(path)
                    # print(xp,yp, 'already taken, skip')
                    continue
                path.append((xp,yp))
                if (xp,yp) in intersections:
                    # print("Found intersection at", xp,yp)
                    return path
            # else:
            #     print('Not valid:', xp,yp)
            #     try:
            #         print("Char:", lines[yp][xp])
            #     except:
            #         pass


        
def find_paths(intersections, lines):
    nodes = []
    visited = set()
    paths = {}
    # for i in intersections:
        # nodes.append((i,None, None, None,None))
    dp = [(-1,0),(1,0),(0,1),(0,-1)]
    # for x,y in intersections:
        # print(x,y)
        # input('wait')

    for x,y in intersections:
        # print("Find paths from:", x,y)
        for dx,dy in dp:
            xp,yp = x+dx, y+dy
            if 0<=xp<len(lines[0]) and 0<=yp<len(lines) and lines[yp][xp]=='.':
                p = find_path([(x,y),(xp,yp)], intersections, lines)
                l = len(p)
                p = [(x,y)]+p
                p1 = p[0]
                p2 = p[-1]
                paths[(p1,p2)]=l
                # print("Path from",x,y,'to', p[-1], 'is', l)
                # print(p)
                
                
            
        # exit(0)
    return paths
    
from collections import defaultdict
def find_greatest_distances(start, end, connections, paths):
    print("Start:", start)
    print("End:", end)
    ncomb = 1
    # for p in connections:
        # ncomb*= len(connections[p])
    # print(ncomb)
    # exit(0)
    to_check = [[start]]
    distances = defaultdict(int)
    visited = set()
    full_paths = []
    max_path = 0
    while len(to_check)>0:
        path = to_check.pop()
        # visited[(tuple(path))]=True
        p = path[-1]
        # print("To check:", len(to_check))
        if p==end:
            # print("Found end, store")
            # print(path)
            full_paths.append(path)
            dist = compute_dist(path, paths)
            if dist>max_path:
                print("Distance:", dist)
                max_path=dist
                print("Max found:", max_path)
            # exit(0)
        # print("Path:", path)
        con = connections[p]
        # print("Connections:", con)
        for c in con:
            if c in path:
                # print("Already in path, skip")
                # exit(0)
                continue
            pp = path.copy()
            pp.append(c)
            dist = compute_dist(pp, paths)
            if tuple(pp) in visited:
                continue
            visited.add(tuple(pp))
            # if (c,dist) in visited:
                # continue
            # visited.add((c,dist))

            # if tuple(pp) in visited:
                # continue
            if pp in to_check:
                print("ERROR")
                exit(0)
            to_check.append(pp)
    return full_paths
    
def compute_dist(p, paths):
    dist = 0
    # for path in paths:
        # print(path)
    for i in range(len(p)-1):
        # print(p[i], p[i+1])
        d = paths[(p[i],p[i+1])]
        dist+=d-1
        # print(d)
    return dist
def solve_day2(fname):
    lines = read_lines(fname)
    lines = remove_slopes(lines)

    intersections = find_intersections(lines)
    start = (1,0)
    end = (len(lines[0])-2, len(lines)-1)
    intersections.append(start)
    intersections.append(end)
    # print(start,end)
    # exit(0)

    paths = find_paths(intersections, lines)
    # print("Paths:", paths)
    connections = {}
    for p,l in paths.items():
        p1,p2 = p
        # print(p1, p2 ,l)
        if p1 not in connections:
            connections[p1]=[]
        if p2 not in connections:
            connections[p2]=[]
        # print("Add p1",p1,"->", p2)
        if p2 not in connections[p1]:
            connections[p1].append(p2)
        if p1 not in connections[p2]:
            connections[p2].append(p1)
    full_paths = find_greatest_distances(start, end, connections, paths)
    max_dist = -1
    for p in full_paths:
        # print(p)
        dist = compute_dist(p, paths)
        # print(dist)
        if dist>max_dist:
            max_dist=dist
    # print(max_dist)
    return max_dist
    # print_path(intersections, lines, full=True)
    # return find_hikes(1,0, lines)

if __name__=='__main__':
    # res1 = solve_day1('input')
    # __builtins__.print("Result part1:", res1)
    res2 = solve_day2('input')

    __builtins__.print("Result part2:", res2)
    # assert res1 == 2210
    assert res2 == 6522
