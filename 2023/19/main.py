import math
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def read_file(fname):
    with open(fname) as file:
        data = file.read()
    return data.strip()

def transpose(lines):
    trans = list(map(list, zip(*lines)))
    lines = []
    for line in trans:
        lines.append(''.join(line))
    return lines

def parse_data(lines):
    data = {} 
    rules = {}
    lines = lines.split('\n\n')
    raw_ins = lines[0]
    raw_ins = raw_ins.split('\n')
    raw_objs = lines[1].split('\n')
    
    for i in raw_ins:
        i = i.split('{')
        name = i[0]
        i = i[1][:-1].split(',')
        iv = []
        for r in i:
            r = r.split(':')
            cond = None
            if len(r)>1:
                cond = r[0]
                n = r[1]
            else:
                n=r[0]
            iv.append({"next":n, "cond":cond})
        rules[name]=iv
    objects = []
    for o in raw_objs:
        o = o[1:-1].split(',')
        om = {}
        for ob in o:
            ob = ob.split('=')
            om[ob[0]] = int(ob[1])
        objects.append(om)
    data['objects']=objects
    data['ins'] = rules
    return data

def is_equal(a1, a2):
    if len(a1)!=len(a2):
        return False
    for i in range(len(a1)):
        if a1[i]!=a2[i]:
            return False
    return True
def check_cond(cond, obj):
    if cond==None:
        return True
    if '<' in cond:
        cond = cond.split('<')
        v = obj[cond[0]]
        cond[1] = int(cond[1])
        if v<int(cond[1]):
            return True
    elif '>' in cond:
        cond = cond.split('>')
        v = obj[cond[0]]
        cond[1] = int(cond[1])
        if v>int(cond[1]):
            return True
    elif '=' in cond:
        print("= in cond, not implemented")
        exit(0)
    else:
        print("cond error")
        exit(0)
    return False

def find_cat(obj, ins):
    instruction = ins['in']
    while True:
        for i in instruction:
            if check_cond(i['cond'], obj):
                ins_name = i['next']
                if ins_name=='A' or ins_name=='R':
                    return ins_name
                instruction = ins[ins_name]
                break
    
def solve_day1(fname):
    lines = read_file(fname)
    data = parse_data(lines)
    ins = data['ins']
    objs = data['objects']
    accepted = []
    for o in objs:
        cat = find_cat(o, ins)
        if cat=='A':
            accepted.append(o)

    score = 0
    for a in accepted:
        for _,v in a.items():
            score+=v

    return score

        
def find_paths(ins):
    to_check = []
    accepted = []
    to_check.append([{'cond':None, 'next': 'in'}])
    while len(to_check)>0:
        p = to_check.pop()
        n = p[-1]['next']
        if n=='R':
            continue
        elif n=='A':
            accepted.append(p)
            continue
        
        instruction = ins[n]

        pp = p.copy()
        for i in instruction:
            cond = i['cond']
            new_path = pp.copy()
            new_path.append(i)
            to_check.append(new_path)
            if cond:
                nins = {'negative':True, "cond":cond}
                pp.append(nins)
    return accepted
        

    
def negate(cond):
    if '<' in cond:
        return cond.replace('<','>')
    return cond.replace('>', '<')

def compute_ranges(path):
    ranges = {"x": [1,4000], "m":[1,4000], "a": [1,4000], "s":[1, 4000]}
    for n in path:
        cond = n['cond']
        if cond:
            if 'negative' in n:
                cond = negate(cond)
                if '<' in cond:
                    cond = cond.split('<')
                    cond[1] = int(cond[1])
                    cond[1] = cond[1]+1
                    cond[1] = str(cond[1])
                    cond = '<'.join(cond)

                elif '>' in cond:
                    cond = cond.split('>')
                    cond[1] = int(cond[1])
                    cond[1] = cond[1]-1
                    cond[1] = str(cond[1])
                    cond = '>'.join(cond)

            if '<' in cond:
                cond = cond.split('<')
                name = cond[0]
                val = int(cond[1])
                if ranges[name][1]-1>val:
                    ranges[name][1]=val-1
            else:
                cond = cond.split('>')
                name = cond[0]
                val = int(cond[1])
                if ranges[name][0]+1<val:
                    ranges[name][0]=val+1

    return ranges

def compute_combinations(ranges):
    nc = 1
    for k,v in ranges.items():
        n = v[1]-v[0]+1
        nc*=n
    return nc
    
def solve_day2(fname):
    lines = read_file(fname)
    data = parse_data(lines)
    ins = data['ins']

    paths = find_paths(ins)
    c = 0
    for p in paths:
        r = compute_ranges(p)
        comb = compute_combinations(r)
        c+=comb
    return c

if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    res2 = solve_day2('input')
    print("Result part2:", res2)
    assert res1 == 319295
    assert res2==110807725108076 
