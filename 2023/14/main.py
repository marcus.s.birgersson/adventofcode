import math
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def print_lines(lines):
    for l in lines:
        print(l)

def transpose(lines):
    trans = list(map(list, zip(*lines)))
    lines = []
    for line in trans:
        lines.append(''.join(line))
    return lines

def parse_data(lines):
    data = [] 
    for l in lines:
        data.append(list(l))
    return data

def is_equal(a1, a2):
    if len(a1)!=len(a2):
        return False
    for i in range(len(a1)):
        if a1[i]!=a2[i]:
            return False
    return True

def tilt_north(lines):
    top_free = [-1]*len(lines[0])
    # print(top_free)
    for y in range(len(lines)):
        for x in range(len(lines[y])):
            if lines[y][x]=='O':
                if top_free[x]>=0:
                    yn = top_free[x]
                    lines[yn][x]='O'
                    lines[y][x]='.'
                    top_free[x]=top_free[x]+1
            elif lines[y][x]=='.':
                if top_free[x]==-1:
                    top_free[x]=y
            elif lines[y][x]=='#':
                top_free[x]=-1
        # print(top_free)
        # exit(0)
    return lines

def compute_north_load(tiles):
    score = 0
    load = len(tiles)
    for l in tiles:
        n = ''.join(l).count('O')
        # print(n)
        score+=load*n
        load-=1
    # print(score)
    return score

def solve_day1(fname):
    lines = read_lines(fname)
    lines = parse_data(lines)
    # for l in lines:
    #     print(l)
    tilted = tilt_north(lines)
    # print('-----')
    # for l in tilted:
    #     print(l)

    score = 0
    load = len(tilted)
    for l in tilted:
        n = ''.join(l).count('O')
        # print(n)
        score+=load*n
        load-=1
    # print(score)
    return score
        
def rotate_right(lst):
    return np.rot90(lst, k=3)
    # return list(zip(*lst[::-1]))
        
def rotate_left(lst):
    # return np.rot270(lst)
    return list(reversed(list(zip(*lst))))

import numpy as np


# mem = {}
def rotate_one_cycle(tiles):
    # if str(tiles) not in mem:
    #     init_tiles = tiles.copy()
    #     # mem[str(tiles)]=True
    # else:
    #     # print("Seen this before")
    #     return mem[str(tiles)]
        # exit(0)
    tiles = tilt_north(tiles)
    # print("Tiles north")
    # print_lines(lines)

    ## Tilt to rotate west
    # tiles = np.rot90(tiles)
    tiles = rotate_right(tiles)
    tiles = tilt_north(tiles)
    # print('Tiles west')
    # print_lines(tiles)
    # exit(0)

    # Tilt to rotate south
    tiles = rotate_right(tiles)
    tiles = tilt_north(tiles)
    # print('Tiles south')
    # print_lines(tiles)

    # Tilt to rotate east
    tiles = rotate_right(tiles)
    tiles = tilt_north(tiles)
    # print('Tiles east')
    # print_lines(tiles)
    
    # mem[str(init_tiles)]=rotate_right(tiles)

  # Rotate back
    return rotate_right(tiles)

def solve_day2(fname):
    lines = read_lines(fname)
    lines = parse_data(lines)
    # for l in lines:
    #     print(l)
    # print('----')
    # for l in rotated:
    #     print(l)
    # Round one
    tiles = lines
    n_cycles = 1000000000
    loops = {}
    init = tiles
    t = tuple(tuple(x) for x in init)
    loops[t]=0

    i = 0
    found = False
    while i<n_cycles:
    # for i in range(n_cycles):
        # if i>12:
            # exit(0)
        # print(i,'/',n_cycles)
        # print_lines(tiles)
        tiles = rotate_one_cycle(tiles)
        
        t = tuple(tuple(x) for x in tiles)
        if str(tiles)==str(init):
            print("Found loop:", i)
            exit(0)
        if t in loops and not found: 
            # print("Found repetition:", i, loops[t])
            # print(str(tiles))
            # print(compute_north_load(tiles))

            diff = i-loops[t]
            left = math.floor((n_cycles-i)/diff)
            # print(left)
            i+=(left*diff)
            # print(i)
            # loops = {} 
            found=True
            # exit(0)
            # while True:
            #     if i+diff<n_cycles:
            #         i+=diff
            #         print(i)
            # exit(0)
        else:
            loops[t]=i
        i+=1

    # print_lines(tiles)
    score = compute_north_load(tiles)
    # print(score)
    return score
    exit(0)
    ## Rotate north
    tiles = rotate_one_cycle(lines)
    print_lines(tiles)

    tiles = rotate_one_cycle(tiles)
    print_lines(tiles)

    tiles = rotate_one_cycle(tiles)
    print_lines(tiles)
    exit(0)

    
    tilted = tilt_north(lines)
    print("Tilted north")
    print_lines(lines)

    ## Tilt to rotate west
    # tilted = np.rot90(tilted)
    tilted = rotate_right(tilted)
    tilted = tilt_north(tilted)
    print('Tilted west')
    print_lines(tilted)
    # exit(0)

    # Tilt to rotate south
    tilted = rotate_right(tilted)
    tilted = tilt_north(tilted)
    print('Tilted south')
    print_lines(tilted)

    # Tilt to rotate east
    tilted = rotate_right(tilted)
    tilted = tilt_north(tilted)
    print('Tilted east')
    print_lines(tilted)
    
  # Rotate back
    tilted = rotate_right(tilted)
    print("After one cycle")
    for l in tilted:
        print(l)
    print('----')


if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    res2 = solve_day2('input')
    print("Result part2:", res2)
    assert res1 == 108826
    assert res2== 99291

    # Wrong: 99698, too high
