import math
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines


def print_map(lines):
    for line in lines:
        for c in line:
            print(c, end='')
        print()

def expand(lines):
    exp = "."*len(lines[0])
    expanded = []
    empty = set(['.'])
    for line in lines:
        s = set(line)
        if s==empty:
            expanded.append(exp)
        expanded.append(line)
    
    return expanded

def transpose(lines):
    trans = list(map(list, zip(*lines)))
    lines = []
    for line in trans:
        lines.append(''.join(line))
    return lines

def expand_map(lines):
    lines = expand(lines)
    trans = transpose(lines)
    trans = expand(trans)
    return transpose(trans)
def find_galaxies(expanded):
    coor = []
    for y in range(len(expanded)):
        for x in range(len(expanded[y])):
            if expanded[y][x]=='#':
                coor.append((x,y))
    return coor
def find_vertical_expansions(lines):
    expansions = []
    empty = set(['.'])
    for line in lines:
        s = set(line)
        if s==empty:
            expansions.append(1)
        else:
            expansions.append(0)
    
    return expansions


def find_expansions(lines):
    vert = find_vertical_expansions(lines)
    hor = find_vertical_expansions(transpose(lines))
    return vert, hor

def compute_dist(galaxies):
    dist = []
    for i in range(len(galaxies)):
        for j in range(i+1, len(galaxies)):
            a = galaxies[i]
            b = galaxies[j]
            d = abs(a[0]-b[0])+abs(a[1]-b[1])
            dist.append((a,b,d, i+1, j+1))
    return dist
def add_expansions(dist, vert, hor, fact=2):
    expanded_dist = []
    for a,b,d,n1, n2 in dist:
        x0 = min(a[0], b[0])
        x1 = max(a[0], b[0])
        y0 = min(a[1], b[1])
        y1 = max(a[1], b[1])
        exp_x = sum(hor[x0+1:x1])
        exp_y = sum(vert[y0+1:y1])
        if exp_x>0:
            d = d+(exp_x)*fact-exp_x
        if exp_y>0:
            d = d+(exp_y)*fact-exp_y
        expanded_dist.append(d)

    return expanded_dist

def solve_day1(fname):
    lines = read_lines(fname)
    vert, hor = find_expansions(lines)
    galaxies = find_galaxies(lines)
    dist = compute_dist(galaxies)
    dist = add_expansions(dist, vert, hor)
    return sum(dist)
        
def solve_day2(fname):
    lines = read_lines(fname)
    vert, hor = find_expansions(lines)
    galaxies = find_galaxies(lines)
    dist = compute_dist(galaxies)
    dist = add_expansions(dist, vert, hor, fact=1000000)
    return sum(dist)

if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    res2 = solve_day2('input')
    print("Result part2:", res2)
    assert res1 == 9214785
    assert res2==613686987427
