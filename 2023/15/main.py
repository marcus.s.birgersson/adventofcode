import math
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def read_data(fname):
    with open(fname) as file:
        data = file.read()
    return data

def transpose(lines):
    trans = list(map(list, zip(*lines)))
    lines = []
    for line in trans:
        lines.append(''.join(line))
    return lines

def parse_data(raw):
    data = []
    for r in raw:
        if '-' in r:
            data.append((r[:-1], '-'))
        elif '=' in r:
            r = r.split('=')
            data.append((r[0], '=', int(r[1])))
    return data

def is_equal(a1, a2):
    if len(a1)!=len(a2):
        return False
    for i in range(len(a1)):
        if a1[i]!=a2[i]:
            return False
    return True

def compute_hash(string):
    h = 0
    for c in string:
        h+=ord(c)
        h*=17
        h%=256
    return h

def solve_day1(fname):
    data = read_data(fname)
    data = data.split(',')
    s  = 0
    for d in data:
        h = compute_hash(d)
        s+=h
    return s
        
def solve_day2(fname):
    data = read_data(fname)
    data = data.split(',')
    data = parse_data(data)
    boxes = {}
    for d in data:
        bnum = compute_hash(d[0])
        if d[1]=='-':
            if bnum in boxes:
                box = boxes[bnum]
            else:
                box = []
            if len(box)>0:
                new_box = []
                for b in box:
                    if b[0]!=d[0]:
                        new_box.append(b)
                boxes[bnum]=new_box
                        
        elif d[1]=='=':
            if bnum in boxes:
                box = boxes[bnum]
            else:
                box = []

                
            is_replaced = False
            for i in range(len(box)):
                if box[i][0]==d[0]:
                    box[i]=d
                    is_replaced=True
                    break
            if not is_replaced:
                box.append(d)

            boxes[bnum]=box
        else:
            print("Error")
            exit(0)
        
    powers = []
    for i in boxes.keys():
        box = boxes[i]
        for j in range(len(box)):
            f = box[j][2]
            p = (i+1)*(j+1)*f
            powers.append(p)

    return sum(powers)

if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    res2 = solve_day2('input')
    print("Result part2:", res2)
    assert res1 == 519041
    assert res2== 260530
