import math
def read_lines(fname):
    with open(fname) as file:
        # lines = [line.rstrip() for line in file]
        data = file.read()
    # print("File data:", data)
    return data
    # return lines

def parse_data(data):
    data = data.split('\n\n')
    seeds = [int(x) for x in data[0].split(": ")[1].split(" ")]

    data = data[1:]
    maps = {}
    for entry in data:
        map_data = entry.split('\n')
        name = map_data[0]
        values = map_data[1:]
        int_values = []
        for v in values:
            v = [int(x) for x in v.split(" ")]
            int_values.append(v)
        maps[name] = int_values
    
    return seeds, maps

def find_mapping(entry, mapping):
    # print(mapping)
    res = entry
    for m in mapping:
        s_min = m[1]
        if entry>=s_min:
            s_max = m[1]+m[2]-1
            if res<=s_max:
                # print("Found mapping for", entry)
                # print("range:", s_min, s_max)
                df = entry-s_min
                res = m[0]+df
                return res
                # print("Mapp", entry, "to", res)
                # print()
    return res
    
def find_reverse_mapping(entry, mapping):
    res = entry
    for m in mapping:
        s_min = m[0]
        if entry>=s_min:
            s_max = m[0]+m[2]-1
            if res<=s_max:
                df = entry-s_min
                res = m[1]+df
                return res
    return res
    

def find_seed_location(seed, maps):
    map_names=[
        "seed-to-soil map:",
        "soil-to-fertilizer map:",
        "fertilizer-to-water map:",
        "water-to-light map:",
        "light-to-temperature map:",
        "temperature-to-humidity map:",
        "humidity-to-location map:"
    ]
    entry = seed
    for name in map_names:
        init = entry
        entry = find_mapping(init, maps[name])
    return entry

# MEM = {}
def find_seed(location, maps):
    global MEM
    map_names=[
        "seed-to-soil map:",
        "soil-to-fertilizer map:",
        "fertilizer-to-water map:",
        "water-to-light map:",
        "light-to-temperature map:",
        "temperature-to-humidity map:",
        "humidity-to-location map:"
    ]
    map_names.reverse()

    entry = location
    for name in map_names:
        # if name not in MEM:
        #     MEM[name]={}
        # if entry in MEM[name]:
        #     entry = MEM[name][entry]
        #     print("Using previous memory")
        # else:
        init = entry
        entry = find_reverse_mapping(init, maps[name])
            # MEM[name][init]=entry
        
    # print(entry)
    return entry

def solve_day1(fname):
    lines = read_lines(fname)
    seeds, maps = parse_data(lines)
    min_location = float('inf')
    for seed in seeds:
        location = find_seed_location(seed, maps)
        if location<min_location:
            min_location = location

    return min_location

def is_valid_seed(seed, seeds):
    i=0
    while i<len(seeds):
        # print(seeds[i], seeds[i+1])
        if seed>=seeds[i] and seed<seeds[i]+seeds[i+1]:
            return True
        i+=2
    return False

    
def solve_day2(fname):
    lines = read_lines(fname)
    seeds, maps = parse_data(lines)

    location = 0

    # location = 227653707 # Correct in first
    # location = 3767628 # Last guess
    # location = 10000000
    while True:
        # print("Check location:", location)
        seed = find_seed(location, maps)
        # print("Seed:", seed)
        # print("Location guess:", location)
        if is_valid_seed(seed, seeds):
            # print("Found seed, min location found:", location)
            return location
            # break
        # else:
        #     print("continue..")
        location+=1


        # break
    # min_location = float('inf')
    # i=0
    # memory = {}
    # while i<len(seeds):
    #     print(seeds[i], seeds[i+1])
    #     seed_init = seeds[i]
    #     for j in range(seeds[i+1]):
    #         if (seed_init+j) in memory:
    #             location = memory[seed_init+j]
    #         else:
    #             location = find_seed_location(seed_init+j,maps)
    #             memory[seed_init+j]=location
    #         if location<min_location:
    #             min_location = location

    #     i+=2
    # print(min_location)
    # return min_location


if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    res2 = solve_day2('input')
    print("Result part2:", res2)
    assert res1==227653707
    assert res2==78775051
