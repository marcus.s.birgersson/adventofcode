import math
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def transpose(lines):
    trans = list(map(list, zip(*lines)))
    lines = []
    for line in trans:
        lines.append(''.join(line))
    return lines

def parse_data(lines):
    data = [] 
    return data

def is_equal(a1, a2):
    if len(a1)!=len(a2):
        return False
    for i in range(len(a1)):
        if a1[i]!=a2[i]:
            return False
    return True

def next_pos(x,y,d):
    if d=='up':
        return (x,y-1,d)
    elif d=='down':
        return (x,y+1,d)
    elif d=='right':
        return (x+1,y,d)
    elif d=='left':
        return (x-1,y,d)

def get_new_direction(d,mirror):
    if mirror=='/':
        if d=='right':
            return 'up'
        elif d=='left':
            return 'down'
        elif d=='up':
            return 'right'
        elif d=='down':
            return 'left'
    elif mirror=='\\':
        if d=='right':
            return 'down'
        elif d=='left':
            return 'up'
        elif d=='up':
            return 'left'
        elif d=='down':
            return 'right'

def move_beam(x,y,d, lines):
    c = lines[y][x]
    if c=='.':
        next_p = next_pos(x,y,d)
        return [next_p]
    elif c=='\\':
        d = get_new_direction(d,'\\')
        next_p = next_pos(x,y,d)
        return [next_p]
    elif c=='/':
        d = get_new_direction(d,'/')
        next_p = next_pos(x,y,d)
        return [next_p]
    elif c=='-':
        if d=='up' or d=='down':
            d1 = 'left'
            d2 = 'right'
            n1 = next_pos(x,y,d1)
            n2 = next_pos(x,y,d2)
            return [n1,n2]
        else:
            next_p = next_pos(x,y,d)
            return [next_p]
    elif c=='|':
        if d=='right' or d=='left':
            d1 = 'up'
            d2 = 'down'
            n1 = next_pos(x,y,d1)
            n2 = next_pos(x,y,d2)
            return [n1,n2]
        else:
            next_p = next_pos(x,y,d)
            return [next_p]
    else:
       print('error!')
       exit(0)

        
def energize_beams(x0,y0,d0, lines):
    visited = set()
    visited.add((x0,y0,d0))
    beams = [(x0,y0,d0)]
    while len(beams)>0:
        next_beams = []
        for beam in beams:
            next_p = move_beam(beam[0], beam[1], beam[2], lines)
            for (x,y,d) in next_p:
                if (x,y,d) in visited:
                    pass
                elif x>=0 and x<len(lines[0]) and y>=0 and y<len(lines):
                    next_beams.append((x,y,d))
                    visited.add((x,y,d))

        beams = next_beams
    energized = set()
    for (x,y,d) in visited:
        energized.add((x,y))
                
    return len(energized)

def solve_day1(fname):
    lines = read_lines(fname)
    n = energize_beams(0,0,'right', lines)

def solve_day2(fname):
    lines = read_lines(fname)
    directions = ['right', 'down', 'up', 'left']
    max_n = 0
    for d in directions:
        if d == 'right':
            X = [0]
            Y = range(len(lines))
        elif d == 'left':
            X = [len(lines[0])-1]
            Y = range(len(lines))
        elif d == 'down':
            X = range(len(lines[0]))
            Y = [0]
        elif d=='up':
            X = range(len(lines[0]))
            Y = [len(lines)-1]

        for y in Y:
            for x in X:
                n = energize_beams(x,y,d,lines)
                if n>max_n:
                    max_n = n

    return max_n

if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    res2 = solve_day2('input')
    print("Result part2:", res2)
    assert res1 ==7860
    assert res2==8331
 
