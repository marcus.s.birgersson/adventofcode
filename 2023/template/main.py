import math
def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def transpose(lines):
    trans = list(map(list, zip(*lines)))
    lines = []
    for line in trans:
        lines.append(''.join(line))
    return lines

def parse_data(lines):
    data = [] 
    return data

def is_equal(a1, a2):
    if len(a1)!=len(a2):
        return False
    for i in range(len(a1)):
        if a1[i]!=a2[i]:
            return False
    return True

def solve_day1(fname):
    lines = read_lines(fname)
    data = parse_data(lines)
        
def solve_day2(fname):
    lines = read_lines(fname)
    data = parse_data(lines)

if __name__=='__main__':
    res1 = solve_day1('input')
    # print("Result part1:", res1)
    # res2 = solve_day2('input')
    # print("Result part2:", res2)
    # assert res1 == 
    # assert res2== 
