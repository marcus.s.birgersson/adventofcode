import math

def read_lines(fname):
    with open(fname) as file:
        lines = [line.rstrip() for line in file]
    return lines

def transpose(lines):
    trans = list(map(list, zip(*lines)))
    lines = []
    for line in trans:
        lines.append(''.join(line))
    return lines

def parse_data(lines):
    data = [] 
    times = lines[0]
    distances = lines[1]
    # times = times.split(" ")[1:]
    times = [int(x) for x in list(filter(lambda x: x!='', times.split(" ")[1:]))]
    distances = [int(x) for x in list(filter(lambda x: x!='', distances.split(" ")[1:]))]
    # print(times)
    # print(distances)
    data = {'times':times, 'dist':distances}
    return data

def is_equal(a1, a2):
    if len(a1)!=len(a2):
        return False
    for i in range(len(a1)):
        if a1[i]!=a2[i]:
            return False
    return True

def compute_dist(v, t):
    return v*t

def find_limits(t, d):
    # print(t,d)
    n=0
    # print("Distance to beat:", d)
    # print("Total time:", t)
    # -> x^2 - Tx -d = 0
    # s = v*T - > s = v*(t-v) -> d = v*(t-v)
    # d = tv -v^2 -> v^2-tv+d = 0
    
    # -> v^2 - tx +d = 0
    min_v = math.ceil((t-math.sqrt(math.pow(t,2)-4*d))/2)
    max_v = math.floor((t+math.sqrt(math.pow(t,2)-4*d))/2)
    # print(min_v, max_v)
    return max_v-min_v+1

    # print("Min v:", min_v)
    # print(compute_dist(min_v, t-min_v))
    # exit(0)
    for i in range(1,t):
        # print(i)
        new_dist = compute_dist(i, t-i)
        if new_dist>d:
            # print("Beating with v=", i)
            n+=1
        elif n>0:
            break
        # print('Hold down:', i, 's -> dist=', new_dist)
    # print("Return:", n)
    # print()
    print("Old n:", n)
    print("Computed n:", max_v-min_v+1)
    exit(0)
    return n

def solve_day1(fname):
    lines = read_lines(fname)
    data = parse_data(lines)
    times = data['times']
    dist = data['dist']
    n = []
    for i in range(len(dist)):
        n.append(find_limits(times[i], dist[i]))
    # print(n)
    res = 1
    for f in n:
        res*=f
    # print(res)
    return res

        
def solve_day2(fname):
    lines = read_lines(fname)
    data = parse_data(lines)
    # print(data)
    times = data['times']
    times = [str(x) for x in times]
    times = int(''.join(times))
    dist = int(''.join([str(x) for x in data['dist']]))
    # print(times)
    # print(dist)
    n = find_limits(times,dist)
    return n

if __name__=='__main__':
    res1 = solve_day1('input')
    print("Result part1:", res1)
    res2 = solve_day2('input')
    print("Result part2:", res2)
    assert res1 == 2269432
    assert res2== 35865985


    # d = v*t
    # T = total time 
    # v = x
    # t = T-x
    # d = x*(T-x) = Tx-x^2
    # -> x^2 - Tx -d = 0
    # x = T/2 pm sqrt()
