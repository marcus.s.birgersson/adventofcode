# importing sys
import sys
import os
 
import argparse
# adding common to the system path
sys.path.insert(0, '../')
 
from common.io import *

DAY = os.getcwd().split('/')[-1]

def format_input(input):
    data = []
    for line in  input:
        line = list(line)
        for i in range(len(line)):
            if line[i]=='-':
                line[i]=-1
            elif line[i]=='=':
                line[i]=-2
            else:
                line[i] = int(line[i])
        line = line[::-1]
        #print("line:", line)
        data.append(line)
    return data
def snafu2dec(snafu):
    dec = 0
    fac = 1
    for s in snafu:
        dec+=(s*fac)
        fac = fac*5
    return dec

def partA(input, expected=None):
    print("Solve for day {:} part A".format(DAY))
    data = format_input(input)
    base_max = 3
    sum = []
    snafu_sum = []
    for d in data:
        print("data:", d)
        for i in range(len(d)):
            while len(sum)<=i+1:
                sum.append(0)
                snafu_sum.append(0)
            sum[i]+=d[i]
            snafu_sum[i]+=d[i]
            print("Sum:", sum)
            print("Snafu:", snafu_sum)
            if snafu_sum[i]>=base_max:
                #print("Sum over ",base_max,":", sum[i])
                #rest = sum[i]//base_max
                #sum[i]=sum[i]%base_max
                #print("Rest:", rest, "New sum:", sum[i])
                #sum[i+1]=sum[i+1]+rest
                print("Positive overflow:")
                print("Init:", snafu_sum)
                init_dec = snafu2dec(snafu_sum)
                #snafu_sum[i]=-2
                snafu_sum[i]-=5
                j=i+1
                while True:
                    while len(snafu_sum)<=j:
                        snafu_sum.append(0)
                    if snafu_sum[j]<base_max-1:
                        snafu_sum[j]+=1
                        break
                    else:
                        snafu_sum[j]=-2
                        j+=1

                #for j in range(i+1,len(snafu_sum)):
                #    if snafu_sum[j]<base_max-1:
                #        snafu_sum[j]+=1
                #        break
                #    else:
                #        snafu_sum[j]=-2
                print("Final:", snafu_sum)
                final_dec = snafu2dec(snafu_sum)
                print("Check positive overflow correction")
                print("init:", init_dec, 'final:', final_dec)
                assert init_dec==final_dec
            if snafu_sum[i]<-2:
                print("Neg error")
                print(snafu_sum)
                init_dec = snafu2dec(snafu_sum)
                snafu_sum[i]=snafu_sum[i]+5
                for j in range(i+1, len(snafu_sum)):
                    if snafu_sum[j]>-2:
                        snafu_sum[j]-=1
                        break
                    else:
                        snafu_sum[j]=2
                final_dec = snafu2dec(snafu_sum)
                print("Init:", init_dec)
                print("Final:", final_dec)
                assert init_dec==final_dec
                print(snafu_sum)
                #quit()


                #snafu_sum[i+1]=snafu_sum
                #snafu_sum[i+1]=snafu_sum[i+1]-2
            if snafu_sum[i+1]<-2:
                print("ERROR:", snafu_sum)
    print("Sum:",sum)
    print("SNAFU sum:", snafu_sum)
    non_zero_found = False
    snafu_str_sum=''
    for c in snafu_sum[::-1]:
        if c==0 and not non_zero_found:
            continue
        else:
            non_zero_found=True
            if c==-2:
                c='='
            elif c==-1:
                c='-'
            else:
                c=str(c)
            snafu_str_sum+=c

    dec = 0
    fac = 1
    for s in sum:
        dec+=(s*fac)
        fac = fac*5
    print(dec)
    print("snafu2dec:", snafu2dec(sum))
    snafu=[0]*10
    answear = snafu_str_sum
    print("Answear:", answear)
    
    # Not correct: 2=01-7-60-=2-8-60-4-51-5-14-130
    # Not correct: 2=--0=012000=0-2-021
    if answear:
        print("Solution for day {:} part A:".format(DAY),answear)
    if expected:
        assert answear==expected

def partB(input, expected=None):
    print("Solve for day {:} part B".format(DAY))
    data = format_input(input)

    answear = None
    if answear:
        print("Solution for day {:} part B:".format(DAY),answear)
    if expected:
        assert answear==expected

def get_input_data(fname, raw):
    try:
        if raw:
            return read_file(fname)
        return read_list_data(fname)
    except FileNotFoundError as e:
        print("Exception:", e)
        return "" if raw else []

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Advent of code day {:}'.format(DAY))
    parser.add_argument('case', type=str, nargs='?', default='all', 
                        choices=['all', 'a', 'A', 'b', 'B'],
                        help='Solution for Advend of Code day')

    parser.add_argument('--filename', '-f', type=str, nargs='?', default='input.txt')
    parser.add_argument('--raw', type=bool, nargs=1, default=False)
    args = parser.parse_args()
    input = get_input_data(args.filename, args.raw)
    case = args.case.lower()
    if case == 'a' or case == 'all':
        partA(input,expected="2=0-2-1-0=20-01-2-20")
    if case == 'b' or case == 'all':
        partB(input)


