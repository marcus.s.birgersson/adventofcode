# importing sys
import sys
import os
 
import argparse
# adding common to the system path
sys.path.insert(0, '../')
 
from common.io import *

DAY = os.getcwd().split('/')[-1]

def format_input(input):
    min_y, min_x=0,0
    max_y,max_x = len(input), len(input[0])
    winds = {}
    start = None
    end = None
    for y in range(min_y, max_y):
        for x in range(min_x, max_x):
            c = input[y][x]
            print(c, end='')
            if c in '<v^>':
                winds[(x,y)]=[c]
            elif y==min_y and c=='.':
                start=(x,y)
            elif y==max_y-1 and c=='.':
                end=(x,y)
        print()
    #print(min_x,'<x<', max_x)
    #print(min_y, '<y<', max_y)
    #print(winds)
    #print(start)
    #print(end)
    return max_x, max_y, start, end, winds

def draw_map(start, end, max_x, max_y, pos, winds):
    for y in range(max_y):
        for x in range(max_x):
            if (x,y)==pos:
                print('E', end='')
            elif (x,y)==start or (x,y)==end:
                print('.', end='')
            elif y==0 or y==max_y-1 or x==0 or x==max_x-1:
                print("#", end='')
            elif (x,y) in winds:
                w = winds[(x,y)]
                #print("w:", w)
                if len(w)>1:
                    c=str(len(w))
                    #c = 'W'
                else:
                    c = w[0]
                #print("c:", c)
                print(c, end='')
            else:
                print('.', end='')
        print()
def get_next_winds(winds, max_x, max_y):
    # Update wind position:
    print("Max x:", max_x, 'Max y:', max_y)
    new_winds = {}
    for wx,wy in winds:
        for dir in winds[(wx,wy)]:
            if dir=='>':
                wx+=1
                if wx>=max_x-1:
                    wx=1
            elif dir=='<':
                wx-=1
                if wx<=0:
                    wx=max_x-2
            elif dir=='^':
                wy-=1
                if wy<=0:
                    wy=max_y-2
            elif dir=='v':
                wy+=1
                if wy>=max_y-1:
                    wy=1
            if (wx,wy) in new_winds:
                new_winds[(wx,wy)].append(dir)
            else:
                new_winds[(wx,wy)]=[dir]
    return new_winds
from collections import deque
def find_path(max_x, max_y, start, end, winds):
    
    state = ([start], winds)
    Q = deque([state])
    SEEN = set() 
    #SEEN[start]=0
    
    best_path = None

    while Q:

        path, winds = Q.popleft()

        #print("Path:", path)
        #print('Len path:', len(path))
        if best_path:
            if len(best_path)<len(path):
                continue

        if str(path) in SEEN:
            print("Already seen, skip")
            quit()
            continue
        SEEN.add(str(path))
        #print(path)
        #print(winds)
        (x,y) = path[-1]
        #print("Current pos:", (x,y))

        if (x,y)==end:
            print("DONE!")
            if best_path:
                if len(best_path)>len(path):
                    best_path=path
            else:
                best_path=path

        # Update wind position:
       # new_winds = {}
       # for wx,wy in winds:
       #     for dir in winds[(wx,wy)]:
       #         if dir=='>':
       #             wx+=1
       #             if wx>=max_x:
       #                 wx=1
       #         elif dir=='<':
       #             wx-=1
       #             if wx<=0:
       #                 wx=max_x-1
       #         elif dir=='^':
       #             wy-=1
       #             if wy<=0:
       #                 wy=max_y-1
       #         elif dir=='v':
       #             wy+=1
       #             if wy>=max_y:
       #                 wy=1
       #         if (wx,wy) in new_winds:
       #             new_winds[(wx,wy)].append(dir)
       #         else:
       #             new_winds[(wx,wy)]=[dir]

        #winds = new_winds
        winds = get_next_winds(winds, max_x, max_y)
        
        next_coor = [(x,y), (x+1,y), (x,y+1), (x-1, y), (x,y-1)]
        valid_coor = []
        for coor in next_coor:
            if coor in winds:
                # Not valid
                continue
            elif coor==start or coor==end:
                valid_coor.append(coor)
            elif coor[0]>=max_x-1 or coor[0]<=0:
                continue
            elif coor[1]>=max_y-1 or coor[1]<=0:
                continue
            else:
                # Check that next wind is not in the way
                dx=coor[0]-x
                dy=coor[1]-y
                if coor==(2,2):
                    print("Current:", x,y)
                    print("Next:", coor)
                    print("Quit")
                    print(coor in winds)
                    draw_map(start, end, max_x, max_y, (x,y), winds)
                    #quit()
                pos = (x,y)
                if dx>0:
                    if pos in winds and '<' in winds[pos]:
                        continue
                elif dx<0:
                    if pos in winds and '>' in winds[pos]:
                        continue
                if dy>0:
                    if pos in winds and '^' in winds[pos]:
                        continue
                elif dy<0:
                    if pos in winds and 'v' in winds[pos]:
                        continue


                valid_coor.append(coor)

        for valid in valid_coor:
            #print("Valid next:", valid)
            Q.append((path+[valid],winds))
        #draw_map(start, end, max_x, max_y, SEEN, winds)


    print("Best path:", best_path)
    print("Len:", len(best_path))
    print("Done with function, returns")
    return best_path


def partA(input, expected=None):
    print("Solve for day {:} part A".format(DAY))
    #data = format_input(input)
    max_x, max_y, start, end, winds = format_input(input)
    w0 = winds
    w1 = get_next_winds(w0, max_x, max_y)
    draw_map(start, end, max_x, max_y, start, w0)
    print()
    draw_map(start, end, max_x, max_y, start, w1)
    print()
    w2 = get_next_winds(w1, max_x, max_y)
    draw_map(start, end, max_x, max_y, start, w2)
    quit()
    path = find_path(max_x, max_y, start, end, winds)
    print("Draw best path:")
    for m in range(len(path)):
        pos = path[m]
        if m==0:
            print("Initial state:")
        else:
            print("Minute", m)

        draw_map(start, end, max_x, max_y, pos, winds)
        winds = get_next_winds(winds, max_x, max_y)

    answear = None
    
    if answear:
        print("Solution for day {:} part A:".format(DAY),answear)
    if expected:
        assert answear==expected

def partB(input, expected=None):
    print("Solve for day {:} part B".format(DAY))
    data = format_input(input)

    answear = None
    if answear:
        print("Solution for day {:} part B:".format(DAY),answear)
    if expected:
        assert answear==expected

def get_input_data(fname, raw):
    try:
        if raw:
            return read_file(fname)
        return read_list_data(fname)
    except FileNotFoundError as e:
        print("Exception:", e)
        return "" if raw else []

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Advent of code day {:}'.format(DAY))
    parser.add_argument('case', type=str, nargs='?', default='all', 
                        choices=['all', 'a', 'A', 'b', 'B'],
                        help='Solution for Advend of Code day')

    parser.add_argument('--filename', '-f', type=str, nargs='?', default='input.txt')
    parser.add_argument('--raw', type=bool, nargs=1, default=False)
    args = parser.parse_args()
    input = get_input_data(args.filename, args.raw)
    case = args.case.lower()
    if case == 'a' or case == 'all':
        partA(input)
    if case == 'b' or case == 'all':
        partB(input)


