# Day       Time  Rank  Score       Time  Rank  Score
#  20   00:11:06    35     66   00:12:04    13     88


from collections import deque, namedtuple

_ = namedtuple("_", ("id", "val"))


def p1(f):
    nums = deque(_(id, int(x)) for id, x in enumerate(f))
    order = list(nums)

    for x in order:
        idx = next(i for i, y in enumerate(nums) if x.id == y.id)
        nums.rotate(-idx)
        nums.popleft()
        nums.rotate(-x.val)
        tmp = nums.pop()
        print("Insert",x[1],'after', tmp[1])
        nums.appendleft(tmp)
        nums.appendleft(x)
        #print(nums)
        #if True or x[1]<0:
        #    print(nums)

    #print(nums)
    nums.rotate(-next(i for i, x in enumerate(nums) if x.val == 0))
    print()
    print(nums[1000 % len(nums)].val, nums[2000 % len(nums)].val, nums[3000 % len(nums)].val)
    
    return nums[1000 % len(nums)].val + nums[2000 % len(nums)].val + nums[3000 % len(nums)].val


def p2(f):
    nums = deque(_(id, int(x) * 811589153) for id, x in enumerate(f))
    order = list(nums)

    for t in range(10):
        for x in order:
            idx = next(i for i, y in enumerate(nums) if x.id == y.id)
            nums.rotate(-idx)
            nums.popleft()
            nums.rotate(-x.val)
            nums.appendleft(x)

    nums.rotate(-next(i for i, x in enumerate(nums) if x.val == 0))
    return nums[1000 % len(nums)].val + nums[2000 % len(nums)].val + nums[3000 % len(nums)].val

import sys
fname = sys.argv[1]
print(fname)
with open(fname) as f:
    data = f.read()
data = [int(d) for d in data.split('\n') if d!='']

res = p1(data)
print("Result:", res)
