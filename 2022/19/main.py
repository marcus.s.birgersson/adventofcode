# importing sys
import sys
import os
 
import argparse
# adding common to the system path
sys.path.insert(0, '../')
 
from common.io import *

from collections import deque
import time

DAY = os.getcwd().split('/')[-1]

def parse_robot_line(robot_str):
    robot_str = robot_str.split(" ")
    robo_type = robot_str[2]
    i = 5
    robot = {'type':robo_type, 'cost':{}}
    cost = {'ore':0, 'clay':0, 'obsidian':0}
    while i<len(robot_str)-1:
        if robot_str[i]=='and':
            i+=1
            continue
        amount = robot_str[i]
        currency = robot_str[i+1]
        cost[robot_str[i+1]]=int(robot_str[i])
        i+=2
    robot['cost']=cost
    return robot

# Types of currency: Ore, Clay, Obsidian
def format_input(input):
    blueprints = []
    for line in input:
        if line=='':
            continue
        blueprint = {}
        line = line.split(":")[1]
        line = line.split(".")
        for robot_string in line:
            if robot_string!='':
                robot = parse_robot_line(robot_string)
                blueprint[robot['type']]=robot
        blueprints.append(blueprint)
        
    return blueprints

def old_do_mining(max_time, blueprint):
    vault = {'ore':0, 'clay':0, 'obsidian':0, 'geode':0}
    robots = {'ore':1, 'clay':0, 'obsidian':0, 'geode':0}

    for t in range(max_time):
        print("== Minute", t+1, "==")

        # Try to build robots:
        new_robots = {'geode':0, 'obsidian':0, 'clay':0, 'ore':0}
        for robot in ['geode', 'obsidian', 'clay', 'ore']:
            bp = blueprint[robot]
            cost = bp['cost']
            can_build = True
            for m in cost: # Might need to make possible for multiple builds
                #print("Cost:", m, cost[m])
                #print("Inventory:", vault[m])
                if vault[m]<cost[m]:
                    #print("Can not build robot of type", robot)
                    can_build = False
                    break
            if can_build:
                #print("Start build robot of type",robot)
                spend_str = ""
                for m in cost:
                    if cost[m]>0:
                        vault[m]=vault[m]-cost[m]
                        if spend_str=="":
                            spend_str+=(str(cost[m]) + " " + m)
                        else:
                            spend_str+=("and "+ str(cost[m]) + " " + m)
                print("Spend", spend_str, "to start building a", robot,'-collecting robot')
                new_robots[robot]+=1
                

        # Collect minerals
        for robot in robots:
            if robots[robot]>0:
                #print("Have",robots[robot], "robot(s) of type",robot)
                #print("Have", vault[robot], 'of', robot,'items')
                vault[robot]+=robots[robot]
                print(robots[robot],robot,'-collecting robot collects', robots[robot], robot,"; you now have", vault[robot], robot)
        
        # If robots been built, they are done now
        for robot in new_robots:
            if new_robots[robot]>0:
                robots[robot]+=new_robots[robot]
                print("The new", robot,"-collecting robot is ready; you have", robots[robot], "of them")
        print()
        if False and t>2:
            quit()
                        
    return vault

def can_build(cost, ore, clay, obs):
    return cost['ore']<=ore and cost['clay']<=clay and cost['obsidian']<=obs
    
def do_mining(max_t, blueprint):
    
    state = (1,0,0,0,0,0,0,0,0) # (ore_robot, clay_robot, obsidian_robot, geode_robot, 
    #  ore, clay, obsidian, geode, t)
    best_state=state
    best_geo = state[8]
    que = deque([state])
    seen = set()

    while que:
        state = que.popleft()

        ore_r, clay_r, obs_r, geo_r, ore, clay, obs, geo, t = state
        if geo>best_geo:
            best_geo = geo
            best_state = state
            print("Best state:", state)


        #print("state:", state)
        # Try to build robots
        do_build = {'ore':False, 'clay':False,'obsidian':False, 'geode':False}
        for robot_type in do_build:
            if can_build(blueprint[robot_type]['cost'], ore, clay, obs):
                do_build[robot_type]=True

        # Collect minerals
        ore+=ore_r
        clay+=clay_r
        obs+=obs_r
        geo+=geo_r

        state = (ore_r, clay_r, obs_r, geo_r, ore, clay, obs, geo, t)

        if state in seen:
            continue
        seen.add(state)
        

        if t>=max_t:
            continue

        # Create one state without building robots
        que.append((ore_r, clay_r, obs_r, geo_r, ore, clay, obs, geo, t+1))
        # If can build robots, create state where building robots:
        if do_build['ore']>0:
           dOre = blueprint['ore']['cost']['ore']
           dClay = blueprint['ore']['cost']['clay']
           dObs = blueprint['ore']['cost']['obsidian']
           que.append((ore_r+1, clay_r, obs_r, geo_r, ore-dOre, clay-dClay, obs-dObs, geo, t+1))
        if do_build['clay']:
           dOre = blueprint['clay']['cost']['ore']
           dClay = blueprint['clay']['cost']['clay']
           dObs = blueprint['clay']['cost']['obsidian']
           que.append((ore_r, clay_r+1, obs_r, geo_r, ore-dOre, clay-dClay, obs-dObs, geo, t+1))
        if do_build['obsidian']:
           dOre = blueprint['obsidian']['cost']['ore']
           dClay = blueprint['obsidian']['cost']['clay']
           dObs = blueprint['obsidian']['cost']['obsidian']
           que.append((ore_r, clay_r, obs_r+1, geo_r, ore-dOre, clay-dClay, obs-dObs, geo, t+1))
        if do_build['geode']:
           dOre = blueprint['geode']['cost']['ore']
           dClay = blueprint['geode']['cost']['clay']
           dObs = blueprint['geode']['cost']['obsidian']
           que.append((ore_r, clay_r, obs_r, geo_r+1, ore-dOre, clay-dClay, obs-dObs, geo, t+1))

   

    print("Final best state:", best_state)
    return best_geo

def partA(input, expected=None):
    print("Solve for day {:} part A".format(DAY))
    blueprints = format_input(input)
    geos = []
    start = time.time()
    for bp in blueprints:
        #bp = blueprints[0]
        s = time.time()
        n_geo = do_mining(24, bp)
        print("Computation time:", time.time()-s)
        geos.append(n_geo)
    print("Total time:", time.time()-start)
    
    print(geos)
    quality_level = 0
    for i in len(geos):
        quality_level+=(geos[i]+(i+1))
    print("Quality level:", quality_level)

    answear = None
    #for bp in blueprints:
    #    print(bp)
    #    print()
    if answear:
        print("Solution for day {:} part A:".format(DAY),answear)
    if expected:
        assert answear==expected

def partB(input, expected=None):
    print("Solve for day {:} part B".format(DAY))
    data = format_input(input)

    answear = None
    if answear:
        print("Solution for day {:} part B:".format(DAY),answear)
    if expected:
        assert answear==expected

def get_input_data(fname, raw):
    try:
        if raw:
            return read_file(fname)
        return read_list_data(fname)
    except FileNotFoundError as e:
        print("Exception:", e)
        return "" if raw else []

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Advent of code day {:}'.format(DAY))
    parser.add_argument('case', type=str, nargs='?', default='all', 
                        choices=['all', 'a', 'A', 'b', 'B'],
                        help='Solution for Advend of Code day')

    parser.add_argument('--filename', '-f', type=str, nargs='?', default='input.txt')
    parser.add_argument('--raw', type=bool, nargs=1, default=False)
    args = parser.parse_args()
    input = get_input_data(args.filename, args.raw)
    case = args.case.lower()
    if case == 'a' or case == 'all':
        partA(input)
    if case == 'b' or case == 'all':
        partB(input)


